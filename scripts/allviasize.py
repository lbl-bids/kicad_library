import pcbnew
import numpy
mil=25.4*1000
def t3():
    b=pcbnew.GetBoard()
    tracks=b.GetTracks()
    vias=[v for v in tracks if v.GetClass()=='PCB_VIA']
    for via in vias:
        via.SetWidth(int(round(21*25.4*1000,10)))
        via.SetDrill(int(round(7*25.4*1000,10)))
    return vias
