#https://en.wikibooks.org/wiki/Kicad/file_formats
import re
import sys
import os.path
import pprint
class c_symbolFline:
	def __init__(self,symbolFlinedict):
		self.field_number=int(symbolFlinedict['field_number'])
		self.text=symbolFlinedict['text']
		self.posX=int(symbolFlinedict['posX'])
		self.posY=int(symbolFlinedict['posY'])
		self.text_size=int(symbolFlinedict['text_size'])
		self.text_orient=symbolFlinedict['text_orient']
		self.visibility=symbolFlinedict['visibility']
		self.htext_justify=symbolFlinedict['htext_justify']
		self.vtext_justify,self.text_style_1,self.text_style_2=symbolFlinedict['vtext_justify']
		self.field_name=symbolFlinedict['field_name']
		self.defaultfield_name={0:'reference',1:'name',2:'Footprint',3:'Datasheet'}
		#if self.field_name==None:
		if self.field_number in self.defaultfield_name:
			self.field_name=self.defaultfield_name[self.field_number]

	def getfield_name(self):
		return self.field_name
	def settext(self,text):
		self.text=text
	def strgen(self):
		return 'F%(field_number)d %(text)s %(posX)d %(posY)d %(text_size)d %(text_orient)s %(visibility)s %(htext_justify)s %(vtext_justify)s %(field_name)s'%({"field_number":self.field_number,'text':self.text,'posX':self.posX,'posY':self.posY,'text_size':self.text_size,'text_orient':self.text_orient,'visibility':self.visibility,'htext_justify':self.htext_justify,'vtext_justify':self.vtext_justify+self.text_style_1+self.text_style_2,'field_name':'' if self.field_number in self.defaultfield_name.keys() else self.field_name})

class c_drawpin:
	def __init__(self,drawpindict):
		self.name=drawpindict['name']
		self.num=drawpindict['num']
		self.posx=int(drawpindict['posx'])
		self.posy=int(drawpindict['posy'])
		self.length=int(drawpindict['length'])
		self.direction=drawpindict['direction']
		self.name_text_size=int(drawpindict['name_text_size'])
		self.num_text_size=int(drawpindict['num_text_size'])
		self.unit_num=int(drawpindict['unit_num'])
		self.convert=drawpindict['convert']
		self.electrical_type=drawpindict['electrical_type']
		self.pin_type=drawpindict['pin_type']
		pass
	def getname(self):
		return self.name
	def strgen(self):
		return 'X %(name)s %(num)s %(posx)d %(posy)d %(length)d %(direction)s %(name_text_size)d %(num_text_size)d %(unit_num)d %(convert)s %(electrical_type)s %(pin_type)s'%({'name':self.name,'num':self.num,'posx':self.posx,'posy':self.posy,'length':self.length,'direction':self.direction,'name_text_size':self.name_text_size,'num_text_size':self.num_text_size,'unit_num':self.unit_num,'convert':self.convert,'electrical_type':self.electrical_type,'pin_type':self.pin_type if self.pin_type is not None else ''})
class c_drawarc:
	def __init__(self,drawarcdict):
		self.posx=int(drawarcdict['posx'])
		self.posy=int(drawarcdict['posy'])
		self.radius=int(drawarcdict['radius'])
		self.start_angle=float(drawarcdict['start_angle'])
		self.end_angle=float(drawarcdict['end_angle'])
		self.unit=int(drawarcdict['unit'])
		self.convert=drawarcdict['convert']
		self.thickness=int(drawarcdict['thickness'])
		self.fill=drawarcdict['fill']
		self.startx=int(drawarcdict['startx'])
		self.starty=int(drawarcdict['starty'])
		self.endx=int(drawarcdict['endx'])
		self.endy=int(drawarcdict['endy'])
		pass
	def strgen(self):
		return 'A %(posx)d %(posy)d %(radius)d %(start_angle)f %(end_angle)f %(unit)d %(convert)s %(thickness)d %(fill)s %(startx)d %(starty)d %(endx)d %(endy)d'%({'posx':self.posx,'posy':self.posy,'radius':self.radius,'start_angle':self.start_angle,'end_angle':self.end_angle,'unit':self.unit,'convert':self.convert,'thickness':self.thickness,'fill':self.fill,'startx':self.startx,'starty':self.starty,'endx':self.endx,'endy':self.endy })
class c_drawtext:
	def __init__(self,drawtextdict):
		self.posx=int(drawtextdict['posx'])
		self.posy=int(drawtextdict['posy'])
		self.text_size=int(drawtextdict['text_size'])
		self.text_type=drawtextdict['text_type']
		self.unit=int(drawtextdict['unit'])
		self.convert=drawtextdict['convert']
		self.text=drawtextdict['text']
		self.text_italic=drawtextdict['text_italic']
		self.text_bold=drawtextdict['text_bold']
		self.text_hjustify=drawtextdict['text_hjustify']
		self.text_vjustify=drawtextdict['text_vjustify']
		pass
	def strgen(self):
		return 'T %(direction)s %(posx)d %(posy)d %(text_size)d %(text_type)s %(unit)d %(convert)s %(text)s %(text_italic)s %(text_bold)s %(text_hjustify)s %(text_vjustify)s'%({'direction':self.direction,'posx':self.posx,'posy':self.posy,'length':self.length,'text_size':self.text_size,'text_type':self.text_type,'unit':self.unit,'convert':self.convert,'text':self.text,'text_italic':self.text_italic,'text_bold':self.text_bold,'text_hjustify':self.text_hjustify,'text_vjustify':self.text_vjustify})
class c_drawrectangle:
	def __init__(self,drawrectangledict):
		self.startx=int(drawrectangledict['startx'])
		self.starty=int(drawrectangledict['starty'])
		self.endx=int(drawrectangledict['endx'])
		self.endy=int(drawrectangledict['endy'])
		self.unit=int(drawrectangledict['unit'])
		self.convert=drawrectangledict['convert']
		self.thickness=int(drawrectangledict['thickness'])
		self.fill=drawrectangledict['fill']
		pass
	def strgen(self):
		return 'S %(startx)d %(starty)d %(endx)d %(endy)d %(unit)d %(convert)s %(thickness)d %(fill)s '%({'startx':self.startx,'starty':self.starty,'endx':self.endx,'endy':self.endy,'unit':self.unit,'convert':self.convert,'thickness':self.thickness,'fill':self.fill})
class c_drawpolyline:
	def __init__(self,drawpolylinedict):
		#print 'drawpolylinedict',drawpolylinedict
		self.drawpolylinedict=drawpolylinedict
		pass
	def strgen(self):
		return 'P %(point_count)d %(unit)d %(convert)s %(thickness)d %(posxys)s %(fill)s'%(self.drawpolylinedict)
		
class c_drawcircle:
	def __init__(self,drawcircledict):
		self.posx=int(drawcircledict['posx'])
		self.posy=int(drawcircledict['posy'])
		self.radius=int(drawcircledict['radius'])
		self.unit=int(drawcircledict['unit'])
		self.convert=drawcircledict['convert']
		self.thickness=int(drawcircledict['thickness'])
		self.fill=drawcircledict['fill']
		pass
	def strgen(self):
		return 'C %(posx)d %(posy)d %(radius)d %(unit)d %(convert)s %(thickness)d %(fill)s '%({'posx':self.posx,'posy':self.posy,'radius':self.radius,'unit':self.unit,'convert':self.convert,'thickness':self.thickness,'fill':self.fill })
	
class c_draw:
	def __init__(self,drawstr):
		self.arcs=[]
		self.circles=[]
		self.polylines=[]
		self.rectangles=[]
		self.texts=[]
		self.pins={}
		self.rearc=re.compile(r'''A
\s+(?P<posx>[-\d]+)
\s+(?P<posy>[-\d]+)
\s+(?P<radius>\d+)
\s+(?P<start_angle>[-\d]+)
\s+(?P<end_angle>[-\d]+)
\s+(?P<unit>\d+)
\s+(?P<convert>\S+)
\s+(?P<thickness>\d+)
\s+(?P<fill>[FfN])
\s+(?P<startx>[-\d]+)
\s+(?P<starty>[-\d]+)
\s+(?P<endx>[-\d]+)
\s+(?P<endy>[-\d]+)
\s*$''',re.X|re.M)
		self.recircle=re.compile(r'''C
\s+(?P<posx>[-\d]+)
\s+(?P<posy>[-\d]+)
\s+(?P<radius>\d+)
\s+(?P<unit>\d+)
\s+(?P<convert>\S+)
\s+(?P<thickness>\d+)
\s+(?P<fill>[FfN])
\s*$''',re.X|re.M)
		self.rerectangle=re.compile(r'''S
\s+(?P<startx>[-\d]+)
\s+(?P<starty>[-\d]+)
\s+(?P<endx>[-\d]+)
\s+(?P<endy>[-\d]+)
\s+(?P<unit>\d+)
\s+(?P<convert>\S+)
\s+(?P<thickness>\d+)
\s+(?P<fill>[FfN])
\s*$''',re.X|re.M)
		self.retext=re.compile('''T
\s+(?P<direction>[0|900])
\s+(?P<posx>[-\d]+)
\s+(?P<posy>[-\d]+)
\s+(?P<text_size>\d+)
\s+(?P<text_type>\S+)
\s+(?P<text>"[\s\S]*?")
\s+(?P<text_italic>[italic|Normal])
\s+(?P<text_bold>[0|1])
\s+(?P<text_hjustify>[CLR])
\s+(?P<text_vjustify>[CBT])
\s*$''',re.X|re.M)
		self.repin=re.compile('''X
\s+(?P<name>\S+)
\s+(?P<num>\S+)
\s+(?P<posx>[-\d]+)
\s+(?P<posy>[-\d]+)
\s+(?P<length>[-\d]+)
\s+(?P<direction>[RLUD])
\s+(?P<name_text_size>\d+)
\s+(?P<num_text_size>\d+)
\s+(?P<unit_num>\d+)
\s+(?P<convert>[012])
\s+(?P<electrical_type>[IOBTPUWwCEN])
\s*(?P<pin_type>[N|I|C|IC|L|CL|V|F|NX])?
\s*$''',re.X|re.M)
		self.repolyline=re.compile('''P
\s+(?P<point_count>\d+)
\s+(?P<unit>\d+)
\s+(?P<convert>\S+)
\s+(?P<thickness>\d+)
\s+(?P<posxys>\([\s\S]*?)
\s+(?P<fill>[FfN])
\s*$''',re.X|re.M)

		arcdicts=[m.groupdict() for m in self.rearc.finditer(drawstr)]
		for arcdict in arcdicts:
			newarc=c_drawarc(arcdict)
			self.arcs.append(newarc)
			#print 'arc',newarc.strgen()
		circledicts=[m.groupdict() for m in self.recircle.finditer(drawstr)]
		for circledict in circledicts:
			newcircle=c_drawcircle(circledict)
			self.circles.append(newcircle)
#			print 'circle',newcircle.strgen()
		polylinedicts=[m.groupdict() for m in self.repolyline.finditer(drawstr)]
		for polylinedict in polylinedicts:
			newpolyline=c_drawpolyline(polylinedict)
			self.polylines.append(newpolyline)
#			print 'polyline',newpolyline.strgen()
		rectangledicts=[m.groupdict() for m in self.rerectangle.finditer(drawstr)]
		for rectangledict in rectangledicts:
			newrectangle=c_drawrectangle(rectangledict)
			self.rectangles.append(newrectangle)
		#	print 'rectangle',newrectangle.strgen()
		textdicts=[m.groupdict() for m in self.retext.finditer(drawstr)]
		for textdict in textdicts:
			newtext=c_drawtext(textdict)
			self.texts.append(newtext)
			#print 'text',newtext.strgen()
		pindicts=[m.groupdict() for m in self.repin.finditer(drawstr)]
		for pindict in pindicts:
			newpin=c_drawpin(pindict)
			self.pins[newpin.getname()]=newpin
			#print 'drawpin',newpin.strgen()
	def pinnames(self):
		return self.pins.keys()
	
	def strgen(self):
		drawstr=[]
		print len(self.rectangles)
		for drawtype in [self.arcs,self.circles,self.polylines,self.rectangles,self.texts]:
			for drawitem in drawtype:
				drawstr.append(drawitem.strgen())
				print 'drawstrgen',drawstr
		for pinname in self.pins.keys():
			drawstr.append(self.pins[pinname].strgen())
		if len(drawstr)>0:
			drawstr.insert(0,'DRAW')
			drawstr.append('ENDDRAW')
		return '\n'.join(drawstr)
class c_deffield:
	def __init__(self,deffielddict):
		self.deffielddict=deffielddict
	def strgen(self):
		return '%(reference)s %(unused)s %(text_offset)s %(draw_pinnumber)s %(draw_pinname)s %(unit_count)s %(units_locked)s %(option_flag)s'%(self.deffielddict)
class c_symbol:
	def __init__(self):
		self.name=None
		self.libname=None
		self.deffield=None
		self.flines={}
		self.fplist=None
		self.draw=None
		self.dcmname=None
		self.dcmdesc=None
		self.dcmkeyword=None
		self.dcmdocfile=None
		self.alias=None
		self.relibname=re.compile("DEF\s+(?P<name>\S+)\s+(?P<symbolstr>[\s\S]*?)\s*ENDDEF",re.M)
		self.redef=re.compile(r'''
\s*(?P<reference>\S+)
\s+(?P<unused>0)
\s+(?P<text_offset>\d+)
\s+(?P<draw_pinnumber>[YN])
\s+(?P<draw_pinname>[YN])
\s+(?P<unit_count>\d+)
\s+(?P<units_locked>[LF])
\s+(?P<option_flag>[NP])
\s*$''',re.X|re.M)
		self.reFline=re.compile(r'''F
\s*(?P<field_number>\d+)
\s+(?P<text>"[\s\S]*?")
\s+(?P<posX>[-\d]+)
\s+(?P<posY>[-\d]+)
\s+(?P<text_size>[-\d]+)
\s+(?P<text_orient>[HV])
\s+(?P<visibility>\S+)
\s+(?P<htext_justify>[LCR])
\s+(?P<vtext_justify>[TCB][NI][NB])
\s*(?P<field_name>\S+)?
\s*$''',re.X|re.M)
		self.realias=re.compile(r'ALIAS\s+(?P<aliasstr>[\s\S]*?)$',re.M)
		self.refplist=re.compile(r'''\$FPLIST\s*(?P<fpliststr>[\s\S]*?)\s*\$ENDFPLIST''',re.X|re.M)
		self.redraw=re.compile(r'''DRAW\s*(?P<drawstr>[\s\S]*?)\s*ENDDRAW$''',re.X|re.M)
		self.redcmH=re.compile(r'$CMP\s+(?P<name>[\s\S]*?)$',re.M)
		self.redcmname=re.compile(r"\$CMP\s+(?P<name>\S+)\s+(?P<dcmstr>[\s\S]*?)\s*\$ENDCMP",re.M)
		self.redcmD=re.compile(r'D\s+(?P<desc>[\s\S]*?)$',re.M)
		self.redcmK=re.compile(r'K\s+(?P<keyword>[\s\S]*?)$',re.M)
		self.redcmF=re.compile(r'F\s+(?P<docfile>[\s\S]*?)\n',re.M)
		pass
	def setname(self,name):
		self.name=name
	def getname(self):
		return self.name
	def pinnames(self):
		return self.draw.pinnames()
	def dcmdecode(self,dcmstr):
		mH=self.redcmH.search(dcmstr)
		mD=self.redcmD.search(dcmstr)
		mK=self.redcmK.search(dcmstr)
		print dcmstr
		mF=self.redcmF.search(dcmstr)
		print mF.groups()
		if mH:
			self.dcmname=mD.group('name')
		if mD:
			self.dcmdesc=mD.group('desc')
		if mK:
			self.dcmkeyword=mK.group('keyword')
		if mF:
			self.dcmdocfile=mF.group('docfile')
	def libdecode(self,symbolstr):
		if symbolstr:
			defdicts=[m.groupdict() for m in self.redef.finditer(symbolstr)]
			mdef=self.redef.search(symbolstr)
			if mdef:
				self.deffield=c_deffield(mdef.groupdict())#defdicts[0]
			else:
				print 'deffield error'
				print symbolstr
			Flinedicts=[m.groupdict() for m in self.reFline.finditer(symbolstr)]
			for flinedict in Flinedicts:
				newfline=c_symbolFline(flinedict)

				self.flines[newfline.getfield_name()]=newfline
				#print 'strgen',newfline.strgen()
			m=self.refplist.search(symbolstr)
			self.fplist=m.group('fpliststr') if m else None
			m=self.redraw.search(symbolstr)
			self.draw=c_draw(m.group('drawstr')) if m else None
			m=self.realias.search(symbolstr)
			self.alias=m.group('aliasstr').split(' ') if m else None
			#print 'libdecode',self.alias
			#print 'draw',self.draw.strgen() if self.draw else ''
		else:
			print 'symbolstr is ',symbolstr
	def libnamedecode(self,libstr):
		mname=self.relibname.search(libstr)
		if mname:
			libname=mname.group('name')
			if self.libname is None:
				self.libname=libname
			else:
				print 'duplicate lib for %s'%libname
			if self.name is None:
				self.name=libname
			symbolstr=mname.group('symbolstr')
		else:
			symbolstr=''
		#print 'libnamedecode',self.name
		return symbolstr
	def dcmnamedecode(self,libstr):
		mname=self.redcmname.search(libstr)
		if mname:
			dcmname=mname.group('name')
			if self.dcmname is None:
				self.dcmname=dcmname
			else:
				print 'duplicate dcm for %s'%dcmname
			if self.name is None:
				self.name=dcmname
			dcmstr=mname.group('dcmstr')
		else:
			dcmstr=''
		return dcmstr
	def mappins(self,pinsdict):
		origpins=self.draw.pins
		newpins={}
		for pinname in pinsdict.keys():
			if pinname in origpins.keys():
				first=True
				for pinnum in pinsdict[pinname]:
					newpin=c_drawpin(dict(name=pinname,num=pinnum,posx=origpins[pinname].posx,posy=origpins[pinname].posy,length=origpins[pinname].length,direction=origpins[pinname].direction,name_text_size=origpins[pinname].name_text_size,num_text_size=origpins[pinname].num_text_size,unit_num=origpins[pinname].unit_num,convert=origpins[pinname].convert,electrical_type=origpins[pinname].electrical_type,pin_type=origpins[pinname].pin_type if first else 'N'))
					newkey='%s_%d'%(pinname,pinnum)
					newpins[newkey]=newpin
					first=False
			else:
				print '%s not in base symbol pin'%pinname
		self.draw.pins=newpins
	def addFline(self,field_name,text,posX=0,posY=0,text_size=10,text_orient='H',visibility='I',htext_justify='C',vtext_justify='C',text_style_1='N',text_style_2='N'):
		field_number=None
		defaultfield_num={'Footprint':2,'Datasheet':3}
		if field_name in defaultfield_num.keys():
			if field_name in self.flines.keys():
				self.flines[field_name].settext('"%s"'%text)
			else:
				field_number=defaultfield_num[field_name]
				flinedict=dict(field_number=field_number,text='"%s"'%text,posX=posX,posY=posY,text_size=text_size,text_orient=text_orient,visibility=visibility,htext_justify=htext_justify,vtext_justify=vtext_justify+text_style_1+text_style_2,field_name='"%s"'%field_name)
				newfline=c_symbolFline(flinedict)
				self.flines[newfline.getfield_name()]=newfline
				print '??? name is there, but default not?'
		else:
			fn0=sorted([self.flines[k].field_number for k in self.flines.keys()])
			fn1=[i for i in fn0 if i>3]
			if len(fn1)==0:
				field_number=4
			elif max(fn1)==len(fn1)+3:
				field_number=max(fn1)+1
			else:
				field_number=max(fn1)+1 # maybe better find gap but leave for later
			flinedict=dict(field_number=field_number,text='"%s"'%text,posX=posX,posY=posY,text_size=text_size,text_orient=text_orient,visibility=visibility,htext_justify=htext_justify,vtext_justify=vtext_justify+text_style_1+text_style_2,field_name='"%s"'%field_name)
			newfline=c_symbolFline(flinedict)
			self.flines[newfline.getfield_name()]=newfline
		print 'in addFline',field_name,text
	def libstrgen(self):
		libstr=[]
		libstr.append('DEF {name} '.format(name=self.name)+self.deffield.strgen())
		for fline in sorted(self.flines.keys(),key=lambda k: self.flines[k].field_number):
			libstr.append(self.flines[fline].strgen())
		if self.alias:
			libstr.append('ALIAS %s'%(' '.join(self.alias)))
		if self.fplist:
			libstr.append('$FPLIST\n%(fplist)s\n$ENDFPLIST'%{'fplist':self.fplist})
		libstr.append(self.draw.strgen())
		libstr.append('ENDDEF')
		return '\n'.join(libstr)
	def dcmstrgen(self):
		dcmstr=[]
		dcmstr.append('D %s'%(self.dcmdesc if self.dcmdesc else self.name))
		dcmstr.append('K %s'%(self.dcmkeyword if self.dcmkeyword else self.name))
		dcmstr.append('F %s'%(self.dcmdocfile if self.dcmdocfile else ''))
		if len(dcmstr)>0:
			dcmstr.insert(0,'$CMP {name}'.format(name=self.name))
			dcmstr.append('$ENDCMP')
		retstr='\n'.join(dcmstr)
		print 'dcmstrgen',retstr
		return retstr

	def getalias(self):
		return self.alias if self.alias is not None else []
	def copy(self):
		libstr=self.libstrgen()
		dcmstr=self.dcmstrgen()
		newsym=c_symbol()
		print 'copy',libstr
		newsym.libdecode(libstr)
		print 'copy',dcmstr
		newsym.dcmdecode(dcmstr)
		print 'copy',newsym.dcmdocfile
		newsym.alias=None
		return newsym
		
class c_lib:
	def __init__(self):
		self.comps={}
		self.compname={}
	def getcompname(self):
		return self.compname.keys()
	def addsymbol(self,symbol):
		self.comps[symbol.getname()]=symbol
		print 'addsymbol',len(self.comps)
		
	def libfromfile(self,filename):
		if os.path.isfile(filename+'.lib'):
			flib=open(filename+'.lib')
			slib=flib.read()
			flib.close()
			self.libfromstr(slib)
		if os.path.isfile(filename+'.dcm'):
			fdcm=open(filename+'.dcm')
			sdcm=fdcm.read()
			fdcm.close()
			self.dcmfromstr(sdcm)
		self.updatecompname()
	def libfromstr(self,slib):
		#relibfile=re.compile("[\r\n]{1}DEF\s+(?P<name>\S+)\s+([\s\S]*?[\r\n]{1}ENDDEF)")
		relibfile=re.compile("[\r\n]{1}(?P<symlib>DEF\s+\S+\s+[\s\S]*?[\r\n]{1}ENDDEF)")
		for libstr in relibfile.findall(slib):
			newsym=c_symbol()
			symbolstr=newsym.libnamedecode(libstr=libstr)
			compname=newsym.getname()
			if compname not in self.comps.keys():
				self.comps[compname]=newsym
			elif self.comps[compname].libname is not None:
				print compname, 'already in the lib'
			else:
				pass
			self.comps[compname].libdecode(symbolstr)
		self.updatecompname()
	def updatecompname(self):
		for compname in self.comps:
			for aliasname in self.comps[compname].getalias():
				self.compname[aliasname]=compname
			self.compname[compname]=compname
	def dcmfromstr(self,sdcm):
		redcmfile=re.compile("[\r\n]{1}\(?<symdcm>$CMP\s+\S+\s+[\s\S]*?[\r\n]{1}\$ENDCMP")
		for dcmstrin in redcmfile.findall(sdcm):
			newsym=c_symbol()
			dcmstr=newsym.dcmnamedecode(dcmstr=dcmstrin)
			compname=newsym.getname()
			if compname not in self.comps.keys():
				self.comps[compname]=newsym
			elif self.comps[compname].dcmname is not None:
				print compname, 'already in the dcm'
			else:
				pass
			self.comps[compname].dcmdecode(dcmstr)
		self.updatecompname()
#	print self.comps.keys()
	def getsymbol(self,name):
		#print name,self.compname[name]
		return self.comps[self.compname[name]] if name in self.compname else None
	def strgen(self):
		libstr=[]
		dcmstr=[]
		for key in self.comps.keys():
			print 'key',key
			libstr.append(self.getsymbol(key).libstrgen())
			dcmstr.append(self.getsymbol(key).dcmstrgen())
		retlibstr='EESchema-LIBRARY Version 2.4\n#encoding utf-8\n{libstr}\n#\n#End Library'.format(libstr='\n'.join(libstr))
		retdcmstr='EESchema-DOCLIB  Version 2.0\n#\n{dcmstr}\n#\n#End Doc Library'.format(dcmstr='\n'.join(dcmstr))
		return (retlibstr,retdcmstr)
#	def dcmstr(self):
#		retstr=[]
#		for key in l.comps.keys():
#			libstr=l.getsymbol(key).libstrgen()
#			retstr.append(libstr.format(name=key))
#		return 'EESchema-LIBRARY Version 2.4\n#encoding utf-8\n{retstr}\n#\n#End Library'.format(retstr='\n'.join(retstr))
			
if __name__=="__main__":
	l=c_lib()
	l.libfromfile(sys.argv[1])
	
	basesymbol=l.getsymbol('hgtest_Nano_v2.x')
	newsymbol=basesymbol.copy()
	print 'here 0',newsymbol.dcmdocfile
	newsymbol.addFline(field_name='testadd',text='abcdefg')
	newsymbol.addFline(field_name='basesymbol',text='hgtest_Nano_v2.x')
	pinmap={'RESET':[1,2], 'D10':[3], 'D11':[4], 'D12':[5]}
	newsymbol.addFline(field_name='pinmap',text=str(pinmap))
	print 'pinname',newsymbol.pinnames()
	newsymbol.mappins(pinmap)
	newsymbol.setname('testsymbolname')
	nlib=c_lib()
	nlib.addsymbol(newsymbol)	
	nlib.updatecompname()
	print 'here 1'	
	(lib,dcm)=nlib.strgen()
	fwrite=sys.argv[2]
	flib=open(fwrite+'.lib','w')
	flib.write(lib)
	flib.close()
	fdcm=open(fwrite+'.dcm','w')
	fdcm.write(dcm)
	fdcm.close()
#	for key in l.comps.keys()[0:2]:
#		dcmstr=l.getsymbol(key).dcmstrgen()
##		print dcmstr.format(name=key)
