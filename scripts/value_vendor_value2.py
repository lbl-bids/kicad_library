import json
import sys
import re
import pcbnew
def digikeyvalue(manufacturer_part_number,footprint,product_description):
    value=manufacturer_part_number
    #if footprint in ['Capacitor_SMD:C_0201_0603Metric','Capacitor_SMD:C_0402_1005Metric','Capacitor_SMD:C_0805_2012Metric','Capacitor_SMD:C_1206_3216Metric']:
    m=re.match('CAP CER (?P<value>\S+F) \S+V\s+\S+\s+\S+',product_description)
    if m:
        value=m.group('value')
    #elif footprint in ['Resistor_SMD:R_0201_0603Metric','Resistor_SMD:R_0402_1005Metric','Resistor_SMD:R_0805_2012Metric']:
    m=re.match('RES[\s\S]*\s+(?P<value>\S+) OHM \S+ \S+W \S+[\s\S]*',product_description)
    if m:
        value=m.group('value')
#    print(manufacturer_part_number,footprint,product_description,value)
    return value
def updatemfr(filename):
    board=pcbnew.LoadBoard(filename)
    footprints=board.GetFootprints()
    notdigikey=[]
    novendor=[]
    newdigipart=[]
    for fp in footprints:
        fieldnames=[fd.GetName() for fd in fp.GetFields()]
        value=fp.GetFieldText('Value')
        footprint=fp.GetFieldText('Footprint')
        if value=='DNP':
            pass
        elif fp.IsDNP():
            pass
        elif 'Vendor' in fieldnames:
            vendor=fp.GetFieldText('Vendor')
            mdigikey=re.match('digikey:(?P<digivendor>\S+)$',vendor)
            digikey=False
            if vendor in digipart:
                vendor=vendor
                digikey=True
            elif mdigikey:
                vendor=mdigikey.group('digivendor')
                digikey=True
            else:
                notdigikey.append(vendor)
            if digikey:
                if vendor in digipart:
                    product_description=digipart[vendor]['product_description']
                    quantity_available=digipart[vendor]['quantity_available']
                    manufacturer_part_number=digipart[vendor]['manufacturer_part_number']
                    #print(digipart[value].keys())
                    manufacturer=digipart[vendor]['manufacturer']['value']
                    valuelib=digikeyvalue(manufacturer_part_number,footprint,product_description)

                 #   print(footprint,product_description,value)
                    if value!=valuelib:
                        fp.SetField('Value',valuelib)
                        print('update value from %s to %s'%(value,valuelib))
                    fp.SetField('Vendor','digikey:%s'%vendor)
                    fp.SetField('manufacturer',manufacturer)
                    fp.SetField('manufacturer_part_number',manufacturer_part_number)
                    valuefield=fp.GetFieldByName('Value')
                    for fieldname in ['Vendor','manufacturer','manufacturer_part_number']:
                        field=fp.GetFieldByName(fieldname)
                        field.SetLayer(valuefield.GetLayer())
                        field.SetVisible(False) #valuefield.IsVisible())
                else:
                    newdigipart.append(vendor)
            #print(fieldnames)
            #print(fieldnames)
        else:
            print('no vendor field %s %s'%(value,footprint))
            novendor.append((footprint,value))

    board.Save(filename)
    return novendor,notdigikey,newdigipart

if __name__=="__main__":
    with open('/home/ghuang/tmp/rfsocdaughter/digipart.json') as djson:
        digipart=json.load(djson)
    filename=sys.argv[1] #"../balunX4/balunX4.kicad_pcb"
    import pathlib
    ext=pathlib.Path(filename).suffix
    print(ext)
    if ext=='.json':
        with open(filename) as jfile:
            panelcfg=json.load(jfile)
        for ib,bcfg in enumerate(panelcfg):
            mousebitedict=dict(left={},bottom={},right={},top={})
            bid,b=list(bcfg.items())[0]
            updatemfr(b['filename'])
    elif ext=='.kicad_pcb': 
        updatemfr(filename)
    else:
        print('what ext? %s'%ext)

#
#import json
#import sys
#import re
#import pcbnew
#if __name__=="__main__":
#    filename=sys.argv[1] #"../balunX4/balunX4.kicad_pcb"
#    board=pcbnew.LoadBoard(filename)
#    footprints=board.GetFootprints()
#    for fp in footprints:
#        footprint=fp.GetFieldText('Footprint')
#        for fd in fp.GetFields():
#            if fd.IsMandatoryField():
#                pass
#            elif fd.GetName() in ['Vendor','manufacturer','manufacturer_part_number']:
#                pass
#            else:
#                fp.RemoveField(fd.GetName())
#        value=fp.GetFieldText('Value')
#        if value in digipart:
#            product_description=digipart[value]['product_description']
#            quantity_available=digipart[value]['quantity_available']
#            manufacturer_part_number=digipart[value]['manufacturer_part_number']
#            #print(digipart[value].keys())
#            manufacturer=digipart[value]['manufacturer']['value']
#            vendor='digikey:%s'%(str(value))
#            value=digikeyvalue(manufacturer_part_number,footprint,product_description)
#
#         #   print(footprint,product_description,value)
#            fp.SetField('Value',value)
#            fp.SetField('Vendor',vendor)
#            fp.SetField('manufacturer',manufacturer)
#            fp.SetField('manufacturer_part_number',manufacturer_part_number)
#            valuefield=fp.GetFieldByName('Value')
#            for fieldname in ['Vendor','manufacturer','manufacturer_part_number']:
#                field=fp.GetFieldByName(fieldname)
#                field.SetLayer(valuefield.GetLayer())
#                field.SetVisible(valuefield.IsVisible())
#
#        elif value=='DNP':
#            pass
#        elif fp.IsDNP():
#            pass
#        else:
#            print('not in digikey',value)
#
#
#        #vendor=fp.GetFieldText('Vendor')
#        #m=re.match("^\s*(?P<value>[\s\S]+?)\s*$",value)
#        #if value in repdict:
#        #    fp.SetField('Value',repdict[value])
#        #    print(bid,b['filename'],value,repdict[value])
#        #elif m:
#        #    fp.SetField('Value',m.group('value'))
#        #    print(bid,b['filename'],value,m.group('value'))
#            
#
#    board.Save(filename)
