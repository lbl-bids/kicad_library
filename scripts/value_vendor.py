import json
import sys
import re
import pcbnew
def digikeyvalue(manufacturer_part_number,footprint,product_description):
    value=manufacturer_part_number
    #if footprint in ['Capacitor_SMD:C_0201_0603Metric','Capacitor_SMD:C_0402_1005Metric','Capacitor_SMD:C_0805_2012Metric','Capacitor_SMD:C_1206_3216Metric']:
    m=re.match('CAP CER (?P<value>\S+F) \S+V\s+\S+\s+\S+',product_description)
    if m:
        value=m.group('value')
    #elif footprint in ['Resistor_SMD:R_0201_0603Metric','Resistor_SMD:R_0402_1005Metric','Resistor_SMD:R_0805_2012Metric']:
    m=re.match('RES[\s\S]*\s+(?P<value>\S+) OHM \S+ \S+W \S+[\s\S]*',product_description)
    if m:
        value=m.group('value')
#    print(manufacturer_part_number,footprint,product_description,value)
    return value
if __name__=="__main__":
    filename=sys.argv[1] #"../balunX4/balunX4.kicad_pcb"
    with open('/home/ghuang/tmp/rfsocdaughter/digipart.json') as djson:
        digipart=json.load(djson)
    board=pcbnew.LoadBoard(filename)
    footprints=board.GetFootprints()
    for fp in footprints:
        footprint=fp.GetFieldText('Footprint')
        for fd in fp.GetFields():
            if fd.IsMandatoryField():
                pass
            elif fd.GetName() in ['Vendor','manufacturer','manufacturer_part_number']:
                pass
            else:
                fp.RemoveField(fd.GetName())
        value=fp.GetFieldText('Value')
        if value in digipart:
            product_description=digipart[value]['product_description']
            quantity_available=digipart[value]['quantity_available']
            manufacturer_part_number=digipart[value]['manufacturer_part_number']
            #print(digipart[value].keys())
            manufacturer=digipart[value]['manufacturer']['value']
            vendor='digikey:%s'%(str(value))
            value=digikeyvalue(manufacturer_part_number,footprint,product_description)

         #   print(footprint,product_description,value)
            fp.SetField('Value',value)
            fp.SetField('Vendor',vendor)
            fp.SetField('manufacturer',manufacturer)
            fp.SetField('manufacturer_part_number',manufacturer_part_number)
            valuefield=fp.GetFieldByName('Value')
            for fieldname in ['Vendor','manufacturer','manufacturer_part_number']:
                field=fp.GetFieldByName(fieldname)
                field.SetLayer(valuefield.GetLayer())
                field.SetVisible(valuefield.IsVisible())

        elif value=='DNP':
            pass
        elif fp.IsDNP():
            pass
        else:
            print('not in digikey',value)


        #vendor=fp.GetFieldText('Vendor')
        #m=re.match("^\s*(?P<value>[\s\S]+?)\s*$",value)
        #if value in repdict:
        #    fp.SetField('Value',repdict[value])
        #    print(bid,b['filename'],value,repdict[value])
        #elif m:
        #    fp.SetField('Value',m.group('value'))
        #    print(bid,b['filename'],value,m.group('value'))
            

    board.Save(filename)
