import pcbnew
import numpy
mil=25.4*1000
def addgroup(board,items):
    v=pcbnew.PCB_GROUP(board)
    for i in items:
        v.AddItem(i)
    board.Add(v)

def via(board,pos,size,drill):
    v=pcbnew.PCB_VIA(board)
    v.SetStart(pcbnew.VECTOR2I(pos))
    v.SetWidth(int(round(size,10)))
    v.SetDrill(int(round(drill,10)))
    board.Add(v)
#    pcbnew.Refresh()
    return v
def arc(board,start,mid,end,width):
    a=pcbnew.PCB_ARC(board)
    board.Add(a)
    a.SetStart(pcbnew.VECTOR2I(start))
    a.SetEnd(pcbnew.VECTOR2I(end))
    a.SetMid(pcbnew.VECTOR2I(mid))
    a.SetWidth(int(round(width)))
#    pcbnew.Refresh()
    return a
def track(board,start,end,width):
    t=pcbnew.PCB_TRACK(board)
    board.Add(t)
    t.SetStart(pcbnew.VECTOR2I(start))
    t.SetEnd(pcbnew.VECTOR2I(end))
    t.SetWidth(int(round(width)))
#    pcbnew.Refresh()
    return t

def x9y(p):
    return p.x*1000000000+p.y
def x9yminmax(p1,p2):
    x9yp1=x9y(p1)
    x9yp2=x9y(p2)
    (minval,maxval) = (p1,p2) if x9yp1<x9yp2 else (p2,p1)
    return (minval,maxval)
def t1(distance=20,offset=20,unit=mil,removeavrtrack=False):
    b=pcbnew.GetBoard()
    tracks=b.GetTracks()
    selectednet={}
    for i,t in enumerate(tracks):
        if (t.IsSelected()):
            net=t.GetNetname()
            if net not in selectednet:
                selectednet[net]=[]
            selectednet[net].append(t)
            #print(i,t,t.GetStart(),t.GetEnd())
    if len(selectednet)==1:
        pass
    elif len(selectednet)==2:
        print(selectednet.keys())
    else:
        exit('select only 1(single ended) or 2 (diffential ended with P/N,+/-) nets')
    #print(selectednet)
    for net,nettrack in selectednet.items():
        nettracksorted=sorted(nettrack,key=lambda t:min(x9y(t.GetStart()),x9y(t.GetEnd())))
        selectednet[net]=nettracksorted
    nets=list(selectednet.keys())
    if len(selectednet)==2:
        avrtrack=[]
        for itrack,track0 in enumerate(selectednet[nets[0]]):
            track1=selectednet[nets[1]][itrack]
            print(track1.GetClass(),track0.GetClass())
            if track0.GetClass()=='PCB_TRACK':
                start0,end0=x9yminmax(track0.GetStart(),track0.GetEnd())
                start1,end1=x9yminmax(track1.GetStart(),track1.GetEnd())
                avrstart=(start0+start1)/2
                avrend=(end0+end1)/2
                avrtrack.append(track(board=b,start=avrstart,end=avrend,width=2*25.4*1000))
            elif track0.GetClass()=='PCB_ARC':
                start0,end0=x9yminmax(track0.GetStart(),track0.GetEnd())
                start1,end1=x9yminmax(track1.GetStart(),track1.GetEnd())
                center0=track0.GetCenter()
                radius0=track0.GetRadius()
                center1=track1.GetCenter()
                radius1=track1.GetRadius()
                avrstart=(start0+start1)/2
                avrend=(end0+end1)/2
                avrcenter=(center0+center1)/2
                avrradius=(radius0+radius1)/2
                startvector=avrstart-avrcenter
                endvector=avrend-avrcenter
                startangle=numpy.arctan2(startvector.x,startvector.y)
                endangle=numpy.arctan2(endvector.x,endvector.y)
                midangle=(startangle+endangle)/2
                midxy=avrcenter+pcbnew.VECTOR2I(int(round(avrradius*numpy.cos(midangle))),int(round(avrradius*numpy.sin(midangle))))
                #track(board=b,start=avrstart,end=avrend,width=2*25.4*1000)
                avrtrack.append(arc(board=b,start=avrstart,mid=midxy,end=avrend,width=2*25.4*1000))
                #print('start0',start0,'end0',end0,'start1',start1,'end1',end1,avrstart,avrend)

        if laal:
            for itrack,track0 in enumerate(avrtrack[0:-1]):
                track1=avrtrack[itrack+1]
                start0,end0=x9yminmax(track0.GetStart(),track0.GetEnd())
                start1,end1=x9yminmax(track1.GetStart(),track1.GetEnd())



    elif len(selectednet)==1:
        avrtrack=selectednet[nets[0]]
    #print(avrtrack)

        #for net,nettrack in selectednet.items():
        #    #print(net,[(t.GetClass(),t.GetStart(),t.GetEnd(),t.GetStart()<t.GetEnd()) for t in selectednet[net]])
        #    for t in nettrack:
        #        if t.GetClass()=='PCB_ARC':
        #            print(net,t.GetMid(),round(t.GetRadius()/25.4),t.GetCenter())
    vias=[]
    for updn in ['up','dn']:
        leftover= 0 #distance*unit
        for t in avrtrack:
            if t.GetClass()=='PCB_TRACK':
                vcomplex,leftover=trackviapos(t,updn=updn,leftoverin=leftover,distance=distance,offset=offset,unit=unit)
                for v in vcomplex:
                    vias.append(via(board=b,pos=pcbnew.VECTOR2I(int(round(v.real)),int(round(v.imag))),size=21*mil,drill=7*mil))
                pass
            elif t.GetClass()=='PCB_ARC':
                vcomplex,leftover=arcviapos(arc=t,updn=updn,leftoverin=leftover,distance=distance,offset=offset,unit=unit)
                for v in vcomplex:
                    vias.append(via(board=b,pos=pcbnew.VECTOR2I(int(round(v.real)),int(round(v.imag))),size=21*mil,drill=7*mil))
                pass
            
            #print(t.GetClass(),'updn',updn,'leftover',leftover,t.GetStart(),t.GetEnd(),len(vcomplex))
    if removeavrtrack:
        for t in avrtrack:
            b.Delete(t)

    pcbnew.Refresh()
    gndnet=b.GetNetsByName()['GND']
    vkept=[]
    for v in vias:
        vnet=v.GetNetname()
        if vnet=='' or vnet=='GND':
            v.SetNet(gndnet)
            vkept.append(v)
        else:
            print(vnet)
            b.Delete(v)
    addgroup(board=b,items=vkept)
    pcbnew.Refresh()
    return vias
def trackviapos(track,leftoverin,updn,distance=20,offset=20,unit=mil):
    start=track.GetStart() 
    end=track.GetEnd() 
    startcomplex=start.x+1j*start.y
    endcomplex=end.x+1j*end.y
    diffcomplex=endcomplex-startcomplex
    angle=numpy.angle(diffcomplex)
    length=abs(diffcomplex)
    step=distance*unit
    npoint=int(round(length/step))
    rarray=numpy.arange(-leftoverin,length,step)[1:]
    #print(rarray)
    if updn=='up':
        rarrayc=rarray+1j*offset*unit
    elif updn=='dn':
        rarrayc=rarray-1j*offset*unit
    if len(rarray)>0:
        leftoverout=length-rarray[-1]
    else:
        leftoverout=length
    v=startcomplex+rarrayc*numpy.exp(1j*angle)
    return v,leftoverout
def arcviapos(arc,leftoverin,updn,distance,offset,unit):
    start2I=arc.GetStart() 
    end2I=arc.GetEnd() 
    center2I=arc.GetCenter()
    radius=arc.GetRadius()
    start=start2I.x+1j*start2I.y
    end=end2I.x+1j*end2I.y
    center=center2I.x+1j*center2I.y
    startvec=start-center
    endvec=end-center
    startangle=numpy.angle(startvec)
    endangle=numpy.angle(endvec)
    diffangle=endangle-startangle
    signdiff=numpy.sign(diffangle)
    if updn=='up':
        r=radius-offset*unit*signdiff
    elif updn=='dn':
        r=radius+offset*unit*signdiff
    else:
        exit(updn)
    leftoverangle=leftoverin/r
    stepangle=distance*unit/r
    a=startangle+numpy.arange(-1*numpy.sign(diffangle)*leftoverangle,diffangle,numpy.sign(diffangle)*stepangle)[1:]
    rarray=r*numpy.exp(1j*a)
    if len(a)>0:
        leftoverout=abs(r*(endangle-a[-1]))
    else:
        leftoverout=abs(endangle-startangle)
    v=center+rarray
    #print('startangle',round(startangle*180/numpy.pi),'endangle',round(endangle*180/numpy.pi),'stepangle',stepangle*180/numpy.pi,(a*180/numpy.pi))
    return v,leftoverout
