import json
import sys
import os
from pathlib import Path
with open(sys.argv[1]) as jfile:
    cfglist=json.load(jfile)
filelist={}
for cfg in cfglist:
    for k,v in cfg.items():
        filename=v['filename']
        prefix=k
        if filename not in filelist:
            filelist[filename]=[]
        filelist[filename].append(k)
index=1
stems=[]
for k,v in filelist.items():
    stem=Path(k).stem
    stems.append(stem)
    if 0:
        print('''mkdir -p {stem}; python ../scripts/gerber.py {pcb} {stem} -bom -xypos; 
    cd {stem}; mv bom.csv {stem}_bom.csv; 
    mv pos_all.csv {stem}_pos_all.csv;
    mv pos_bottom.csv {stem}_pos_bottom.csv;
    mv pos_top.csv {stem}_pos_top.csv;
    cd ..;'''.format(**dict(stem=stem,pcb=k)))
    print('%d;%s;%d;%s'%(index,stem,len(v),','.join(v)))
    index=index+1
print(' '.join(stems))




