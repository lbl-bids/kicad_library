import numpy
import pcbnew
# fp1.m_Uuid.Clone(fp.m_Uuid)
def uuidclone(src,dst,uuidinc=0):
    if isinstance(src,pcbnew.KIID):
        srcuuid=src
    elif hasattr(src,'m_Uuid'):
        srcuuid=src.m_Uuid
    elif src is None:
        srcuuid=pcbnew.KIID()
    else:
        exit('unknown uuid')
    for i in range(uuidinc):
        srcuuid.Increment()
    dst.m_Uuid.Clone(srcuuid)
def fpinfo(fp):
    layer=fp.GetLayer()
    position=fp.GetPosition()
    reference=fp.GetReference()
    return (reference,layer,(position.x,position.y))
def trackinfo(t):
    trackclass=t.GetClass()
    points=[]
    points.append(t.GetStart())
    if trackclass=='PCB_TRACK':
        points.append(t.GetEnd())
    elif trackclass=='PCB_ARC':
        points.append(t.GetMid())
        points.append(t.GetEnd())
    elif trackclass=='PCB_VIA':
        pass
    else:
        print('what track class %s'%trackclass)
    pointslist=[(i.x,i.y) for i in points]
    layer=t.GetLayer()
    width=t.GetWidth()
    net=t.GetNet()
    return (pointslist,layer,width,net)

def zonecornerarray(z):
    numcorners=z.GetNumCorners()
    xyarray=numpy.zeros((numcorners,2))
    for i in range(numcorners):
        corner=z.GetCornerPosition(i)
        xyarray[i]=(corner.x,corner.y)
    return xyarray.tolist()
def padinfo(p):
    name=p.GetName()
    position=p.GetPosition()
    size=p.GetSize()
    layer=p.GetLayer()
    roundrect_rratio=p.GetRoundRectRadiusRatio()
    netname=p.GetNetname()
    return (name,(position.x,position.y),(size.x,size.y),layer,roundrect_rratio,netname)
def fplineinfo(g):
    position=g.GetPosition()
    layer=g.GetLayer()
    return ((position.x,position.y),layer)
def fpzoneinfo(z):
    layer=z.GetLayer()
    netname=z.GetNetname()
    corner=zonecornerarray(z)
    return (layer,netname,corner)

def oneboard(newboard,netprefix,filename,rotorigin,rotdeg,lowerleft,edgeonly=False,dnp=None,uniqueref=False,refill=False,uuidincref=None,uuidinc=None,debug=False):
    if debug:
        print('new oneboard',uuidincref.AsString())
    board=pcbnew.LoadBoard(filename)
    copperlayercount=board.GetCopperLayerCount()
    copperlayercountnew=newboard.GetCopperLayerCount()
    if copperlayercount!=copperlayercountnew:
        exit('layer count different %d %d'%(copperlayercount,copperlayercountnew))
    else:
        edgecut=[]
        drawings=[]
        for d in board.GetDrawings():
            if (d.GetLayerName()=="Edge.Cuts"):
                edgecut.append(d)
            else:
                drawings.append(d)
        for e in edgecut:
            e.Rotate(rotorigin,rotdeg)

        exs=[min([c.x for c in e.GetCorners()]) for e in edgecut]
        eys=[max([c.y for c in e.GetCorners()]) for e in edgecut]
        if len(edgecut)==1:
            ex=exs[0]
            ey=eys[0]
        else:
            ex=min(ex)
            ey=max(ey)
        offset=lowerleft-pcbnew.VECTOR2I(ex,ey)
        for e in edgecut:
            e.Move(offset)
            if edgeonly:
                newboard.Add(e)
            else:
                e.SetLayer(newboard.GetLayerID("F.Silkscreen"))
                uuidclone(src=uuidincref,dst=e,uuidinc=uuidinc)
                newboard.Add(e)
        exs=[max([c.x for c in e.GetCorners()]) for e in edgecut]
        eys=[min([c.y for c in e.GetCorners()]) for e in edgecut]
        if len(edgecut)==1:
            ex=exs[0]
            ey=eys[0]
        else:
            ex=min(ex)
            ey=max(ey)
        upperright=pcbnew.VECTOR2I(ex,ey)
        if edgeonly:
            pass
        else:
            nets=sorted(board.GetNetInfo().NetsByName(),key=lambda x:str(x))
            footprints=sorted(board.GetFootprints(),key=lambda x:fpinfo(x))
            tracks=sorted(board.GetTracks(),key=lambda x:trackinfo(x))
            zones=sorted(board.Zones(),key=lambda x:zonecornerarray(x))
            for ne in nets:
                if ne:
                    ne1name=netprefix+str(ne)
                    ne1=pcbnew.NETINFO_ITEM(board,ne1name)
                    newboard.Add(ne1)
            newboardnets=newboard.GetNetsByName()
            for fp in footprints:
                fp1=fp.Duplicate()
                s1=fp.m_Uuid.AsString()
                s2=fp1.m_Uuid.AsString()
               # fp1.m_Uuid.Clone(fp.m_Uuid)
                #uuidclone(src=fp,dst=fp1,uuidinc=uuidinc)
                uuidclone(src=uuidincref,dst=fp1,uuidinc=uuidinc)
                s3=fp1.m_Uuid.AsString()
                #print('%s %s  %s  %s'%(fp.GetReference(),s1,s2,s3))
                for  fe in fp.GetFields():
                    fe1=fp1.GetFieldByName(fe.GetName())
                    #fe1.m_Uuid.Clone(fe.m_Uuid)
                    #uuidclone(src=fe,dst=fe1,uuidinc=uuidinc)
                    uuidclone(src=uuidincref,dst=fe1,uuidinc=uuidinc)
                pads1=sorted(fp1.Pads(),key=lambda x:padinfo(x))
                pads=sorted(fp.Pads(),key=lambda x:padinfo(x))
                for ipad,pad1 in enumerate(pads1):
                    pad1.SetZoneConnection(pads[ipad].GetZoneConnection())
                    pad1.SetNet(newboardnets[netprefix+pad1.GetNetname()])
                    for i in range(32):
                        pad1.SetZoneLayerOverride(i,pads[ipad].GetZoneLayerOverride(i))

                    #pad1.m_Uuid.Clone(pads[ipad].m_Uuid)
                    #uuidclone(src=pads[ipad],dst=pad1,uuidinc=uuidinc)
                    uuidclone(src=uuidincref,dst=pad1,uuidinc=uuidinc)
                graphs1=sorted(fp1.GraphicalItems(),key=lambda x:fplineinfo(x))
                graphs=sorted(fp.GraphicalItems(),key=lambda x:fplineinfo(x))
                for ig,g in enumerate(graphs1):
                    #g.m_Uuid.Clone(graphs[ig].m_Uuid)
                    #uuidclone(src=graphs[ig],dst=g,uuidinc=uuidinc)
                    uuidclone(src=uuidincref,dst=g,uuidinc=uuidinc)
                fpzones1=sorted(fp1.Zones(),key=lambda x:fpzoneinfo(x))
                fpzones=sorted(fp.Zones(),key=lambda x:fpzoneinfo(x))
                for iz,z in enumerate(fpzones1):
                    #z.m_Uuid.Clone(zones[iz].m_Uuid)
                    #uuidclone(src=zones[iz],dst=z,uuidinc=uuidinc)
                    uuidclone(src=uuidincref,dst=z,uuidinc=uuidinc)

                if uniqueref:
                    fp1.SetReference(netprefix+'_'+fp1.GetReference())
                fp1.Rotate(rotorigin,rotdeg)
                fp1.Move(offset)
                #print(fp1.GetReference())
                newboard.Add(fp1)
            #pads=newboard.GetPads()    


                value=fp.GetValue()
                fp1.SetDNP(aDNP=dnp or fp.IsDNP() or value=='DNP')
                fp1.SetExcludedFromBOM(aExclude=dnp or fp.IsExcludedFromBOM() or value=='DNP')
                fp1.SetExcludedFromPosFiles(aExclude=dnp or fp.IsExcludedFromPosFiles() or value=='DNP')
            for tr in tracks:
                tr1=tr.Duplicate()
                #tr1.m_Uuid.Clone(tr.m_Uuid)
                #uuidclone(src=tr,dst=tr1,uuidinc=uuidinc)
                uuidclone(src=uuidincref,dst=tr1,uuidinc=uuidinc)
                tr1.SetNet(newboardnets[netprefix+tr.GetNetname()])
                tr1.Rotate(rotorigin,rotdeg)
                tr1.Move(offset)
                newboard.Add(tr1)
            if refill:
                if not all([zo.IsFilled() or zo.GetIsRuleArea() for zo in zones]):
                    zonefiller=pcbnew.ZONE_FILLER(board)
                    zonefiller.Fill(zones)
                    if debug:
                        print("refill zones")
                else:
                    pass
            for zo in zones:
                zo1=zo.Duplicate()
                #zo1.m_Uuid.Clone(zo.m_Uuid)
                #uuidclone(src=zo,dst=zo1,uuidinc=uuidinc)
                uuidclone(src=uuidincref,dst=zo1,uuidinc=uuidinc)
                zo1.SetNet(newboardnets[netprefix+zo.GetNetname()])
                zo1.Rotate(rotorigin,rotdeg)
                zo1.Move(offset)
                newboard.Add(zo1)
            for dr in drawings:
                dr1=dr.Duplicate()
                #dr1.m_Uuid.Clone(dr.m_Uuid)
                #uuidclone(src=dr,dst=dr1,uuidinc=uuidinc)
                uuidclone(src=uuidincref,dst=dr1,uuidinc=uuidinc)
                dr1.Rotate(rotorigin,rotdeg)
                dr1.Move(offset)
                newboard.Add(dr1)
    return lowerleft,upperright
if __name__=="__main__":                
    ifilename="slot/slot.kicad_pcb"
    ofilename='slot/slotdup.kicad_pcb'
    netprefix='_b1'
    #netprefix=''
    rotorigin=pcbnew.VECTOR2I(0,0)
    rotdeg=pcbnew.EDA_ANGLE(0,pcbnew.DEGREES_T)
    lowerleft=pcbnew.VECTOR2I(0,0)
    b1=pcbnew.NewBoard(ofilename)
    copperlayercount=6
    b1.SetCopperLayerCount(copperlayercount)
    ur=oneboard(newboard=b1
,netprefix='_b1'
,filename='../diffcarrier/diffcarrier.kicad_pcb'
,rotorigin=rotorigin
,rotdeg=rotdeg
,lowerleft=lowerleft
,edgeonly=0
)
    ur=oneboard(newboard=b1
,netprefix='_b2'
,filename=ifilename
,rotorigin=rotorigin
,rotdeg=rotdeg
,lowerleft=pcbnew.VECTOR2I(ur.x+3000000,lowerleft.y)
,edgeonly=0)
    #print(ur)
    b1.Save(ofilename)
