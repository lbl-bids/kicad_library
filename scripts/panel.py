import time
import re
import pcbnew
import json
import oneboard
import numpy
from mousebite import mousebite
from pcbtools import *
# breadkout vscore: null mouse bite: -1 square open, -2 within boundary, -3 beyound boundary
if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='jsonfile',help='json file (json)')
    parser.add_argument('-out', dest='outfile',default='panel.kicad_pcb',help='output file (kicad_pcb)')
    parser.add_argument('-edgeonly', dest='edgeonly',action='store_true',help='only process the edge cut, to check the breakout')
    parser.add_argument('-uniqueref', dest='uniqueref',action='store_true',help='add board index to each component reference, change the silkscreen')
    parser.add_argument('-refill', dest='refill',action='store_true',help='refill each board before panelize them')
    parser.add_argument('-uuid', dest='uuid',action='store_true',help='change the uuid')
    parser.add_argument('-keepdaterevetc', dest='keepdaterevetc',action='store_true',help='keep date rev etc in gerber/drill')
    parser.add_argument('-debug', dest='debug',action='store_true',help='print debug message')
    clargs=parser.parse_args()

    if clargs.uuid:

        uuidincref=pcbnew.KIID(0)
        trykiid=0
        uuidstr=uuidincref.AsString()
        m=re.match('(?P<head>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4})-000000000000',uuidstr)
        while m is None:
            time.sleep(0.1)
            uuidincref=pcbnew.KIID(0)
            uuidstr=uuidincref.AsString()
            print(uuidstr)
            m=re.match('(?P<head>[0-9a-f]{8})-0000-0000-0000-000000000000',uuidstr)
            trykiid=trykiid+1
        uuidreplacestr=m['head']
        if clargs.debug:
            print(uuidstr,trykiid)
    else:
        uuidincref=None
        uuidreplacestr=None

    edgeonly=clargs.edgeonly
    ofilename=clargs.outfile #'panel.kicad_pcb'
    with open(clargs.jsonfile) as jfile:
        panelcfg=json.load(jfile)
    boardcount={} 
    b1=pcbnew.NewBoard(ofilename)
    copperlayercount=6
    b1.SetCopperLayerCount(copperlayercount)

    llx=0;lly=0
    urx=0;ury=0;
    llbx=0;llby=0;
    urbx=0;urby=0;
    lastref=None
    boardcorner={}
    halfvscore=300000
    halfmousebite=1500000
    panelxmax=int(round(16*25.4*1000000,10))
    panelymax=int(round(22*25.4*1000000,10))
    edgecutsrect(b1,pcbnew.VECTOR2I(0,0),pcbnew.VECTOR2I(panelxmax,-panelymax),100000,uuidincref=uuidincref)
    hlinelist=[]
    vlinelist=[]
    for ib,bcfg in enumerate(panelcfg):
        mousebitedict=dict(left={},bottom={},right={},top={})
        bid,b=list(bcfg.items())[0]
        if clargs.debug:
            print(bid)
        if 'filename' in b:
            boardfilename=b['filename']
            if boardfilename in boardcount:
                boardcount[boardfilename]=boardcount[boardfilename]+1
            else:
                boardcount[boardfilename]=0
        else:
            exit('board filename not in cfg')


        if 'netprefix' in b:
            netprefix=b['netprefix']
        else:
            netprefix='_%s'%bid
        if 'rotorigin' in b:
            rotorigin=pcbnew.VECTOR2I(b['rotorigin'][0],b['rotorigin'][1])
        else:
            rotorigin=pcbnew.VECTOR2I(0,0)
        if 'rotdeg' in b:
            rotdeg=pcbnew.EDA_ANGLE(b['rotdeg'],pcbnew.DEGREES_T)
        else:
            rotdeg=pcbnew.EDA_ANGLE(0,pcbnew.DEGREES_T)
        if 'refboard' in b:
            refx,refy=b['refboard']
        else:
            refx,refy=(lastref,lastref)
        if 'lowerleft' in b:
            lowerleft=pcbnew.VECTOR2I(b['lowerleft'][0],b['lowerleft'][1])
        elif 'deltalowerleftx' in b:
            lowerleft=pcbnew.VECTOR2I(boardcorner[refx]['ur'].x+b['deltalowerleftx'],boardcorner[refy]['ll'].y)
        elif 'deltalowerlefty' in b:
            lowerleft=pcbnew.VECTOR2I(boardcorner[refx]['ll'].x,boardcorner[refy]['ur'].y-b['deltalowerlefty'])
        elif 'deltalowerleft' in b:
            lowerleft=pcbnew.VECTOR2I(boardcorner[refx]['ur'].x+b['deltalowerleft'][0],boardcorner[refy]['ur'].y-b['deltalowerleft'][1])
        elif 'xyoffset' in b:
            idx,idy=b['xyoffset']

            lowerleftx=boardcorner[refx]['llbreakout'].x if idx==0 else boardcorner[refx]['urbreakout'].x
            lowerlefty=boardcorner[refy]['llbreakout'].y if idy==0 else boardcorner[refy]['urbreakout'].y
            lowerleft=pcbnew.VECTOR2I(lowerleftx,lowerlefty)
        else:
            print('on else?')
            lowerleft=pcbnew.VECTOR2I(0,0)
        if 'breakout' in b:
            [left,bottom,right,top]=b['breakout']
            if left is None: # NA
                llbx=lowerleft.x
                llx=lowerleft.x
            elif isinstance(left,list): # mouse bite
                llbx=lowerleft.x
                llx=llbx+halfmousebite
                mousebitedict['left'].update(dict(x=llbx,y=left))
            elif left==0: # vscore
                llbx=lowerleft.x
                llx=llbx+halfvscore
                vlinelist.append(llbx)
            else:
                exit('left %s'%str(left))
            if bottom is None:
                llby=lowerleft.y
                lly=lowerleft.y
            elif isinstance(bottom,list):
                llby=lowerleft.y
                lly=llby-halfmousebite
                mousebitedict['bottom'].update(dict(x=bottom,y=llby))
            elif bottom==0:
                llby=lowerleft.y
                lly=llby-halfvscore
                hlinelist.append(llby)
            else:
                exit('bottom %s'%str(bottom))
            lowerleft=pcbnew.VECTOR2I(llx,lly)
        if 'DNP' in b:
            dnp=b['DNP']
            if clargs.debug:
                print('dnp',dnp)
        else:
            dnp=None

            
        ll,ur=oneboard.oneboard(newboard=b1
,netprefix=netprefix
,filename=boardfilename
,rotorigin=rotorigin
,rotdeg=rotdeg
,lowerleft=lowerleft
,edgeonly=edgeonly
,dnp=dnp
,uniqueref=clargs.uniqueref
,refill=clargs.refill
,uuidincref=uuidincref
,uuidinc=1#boardcount[boardfilename]
,debug=clargs.debug                                
)
        llx=ll.x
        lly=ll.y
        urx=ur.x
        ury=ur.y
        if 'breakout' in b:
            if top is None:
                urby=ury
            elif isinstance(top,list):
                urby=ury-halfmousebite
                mousebitedict['top'].update(dict(x=top,y=urby))
            elif top==0:
                urby=ury-halfvscore
                hlinelist.append(urby)
            else:
                exit('top %s'%str(top))
            if right is None:
                urbx=urx
            elif isinstance(right,list):
                urbx=urx+halfmousebite
                mousebitedict['right'].update(dict(x=urbx,y=right))
            elif right==0:
                urbx=urx+halfvscore
                vlinelist.append(urbx)
            else:
                exit('right %s'%str(right))
            boardcorner[bid]=dict(ll=pcbnew.VECTOR2I(llx,lly),ur=pcbnew.VECTOR2I(urx,ury),urbreakout=pcbnew.VECTOR2I(urbx,urby),llbreakout=pcbnew.VECTOR2I(llbx,llby))
        else:
            print('update bid 1',bid)
            boardcorner[bid]=dict(ll=pcbnew.VECTOR2I(llx,lly),ur=pcbnew.VECTOR2I(urx,ury),urbreakout=pcbnew.VECTOR2I(urx,ury),llbreakout=pcbnew.VECTOR2I(llx,lly))
        #print(mousebitedict)

        lastref=bid
        for eddir in ['left','bottom','right','top']:
            dic=mousebitedict[eddir]
            if dic:
                mousebite(board=b1,x=dic['x'],y=dic['y'],boardcorner=boardcorner[bid],halfmousebite=halfmousebite,uuidincref=uuidincref,debug=clargs.debug)
        if edgeonly:
            pcbtext(board=b1,x=(llx+urx)/2,y=(lly+ury)/2,text='%s\n%s'%(bid,boardfilename),size=pcbnew.VECTOR2I(1000000,2000000))
    vline=sorted(set(vlinelist))
    hline=sorted(set(hlinelist))
    for v in vline:
        edgecutssegment(b1,pcbnew.VECTOR2I(v,0),pcbnew.VECTOR2I(v,-panelymax),100000,uuidincref=uuidincref)
    for h in hline:
        edgecutssegment(b1,pcbnew.VECTOR2I(0,h),pcbnew.VECTOR2I(panelxmax,h),100000,uuidincref=uuidincref)
    


    b1.Save(ofilename)
    if clargs.uuid:
        with open(ofilename) as f:
            s=f.read()
        s1=s.replace('(uuid "%s'%uuidreplacestr,'(uuid "00000000-0000-0000-0000')
        with open(ofilename,'w') as f:
            f.write(s1)
    if clargs.debug:
        print(json.dumps({k:boardcount[k]+1 for k in sorted(boardcount)},indent=4))
