import pcbnew
import sys
b=pcbnew.LoadBoard(sys.argv[1])
fps=b.GetFootprints()
for fp in fps:
    fields=fp.GetFields()
    fdict={f.GetName():f for f in fields}
    for fname in ['Vendor','manufacturer','manufacturer_part_number']:
        if fname in fdict:
            fdict[fname].SetVisible(False)
b.Save(sys.argv[1])
