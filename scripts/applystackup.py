import re
import json

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='proj',help='project name, no extension')
    parser.add_argument('-netclass', dest='netclass',default=None,type=str,help='path for the netclass.json, will be replaced in .kicad_pro net_setting')
    parser.add_argument('-pattern', dest='pattern',default=None,type=str,help='path for the netclass_pattern.json, will be replaced in .kicad_pro net_setting netclasspattern')
    parser.add_argument('-stackup', dest='stackup',default=None,type=str,help='path for the stackup, will be replaced in .kicad_pcb stackup session')
    clargs=parser.parse_args()

    if clargs.stackup is not None:
        with open(clargs.stackup) as f:
            stackup=f.read()
        with open(clargs.proj+'.kicad_pcb') as f:
            originalpcb=f.read()
        updatestackup= re.sub(r'\(stackup(?:[^()]*|\((?:[^()]*|\([^()]*\))*\))*\)', stackup,originalpcb)
        with open(clargs.proj+'.kicad_pcb','w') as f:
            f.write(updatestackup)
    if clargs.netclass is not None:
        with open(clargs.netclass) as f:
            netclass=json.load(f)
        with open(clargs.proj+'.kicad_pro') as f:
            originalpro=json.load(f)
        originalpro['net_settings'].update(netclass)
        with open(clargs.proj+'.kicad_pro','w') as f:
            json.dump(originalpro,f,indent=4)

    if clargs.pattern is not None:
        with open(clargs.pattern) as f:
            pattern=json.load(f)
        with open(clargs.proj+'.kicad_pro') as f:
            originalpro=json.load(f)
        originalpro['net_settings'].update(pattern)
        with open(clargs.proj+'.kicad_pro','w') as f:
            json.dump(originalpro,f,indent=4)

