import pcbnew
import sys
import re
if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(dest='proj',help='project name, do not include extension, the pcb will be proj.kicad_pcb')
    parser.add_argument('-report', dest='reportfile',default='report.txt',help='report file from annotation save')
    clargs=parser.parse_args()
    proj=clargs.proj
    pcbfilename=proj+'.kicad_pcb'
    b=pcbnew.LoadBoard(pcbfilename)
    footprints=b.GetFootprints()
    srcfpdict={fp.GetReference():fp for fp in footprints}
    dstfpdict={}
    with open(clargs.reportfile) as reportfile:
        s=reportfile.read()
    for l in s.split('\n'):
        if l:
            m=re.match(r"Updated\s+(?P<net>\S+)\s+(\(unit [A-Z]\))?\s*from\s+(?P<from>\S+)\s+to\s+(?P<to>\S+).\s*",l)
            if m:
                fromfootprint=m.group('from') 
                netfootprint=m.group('net')
                tofootprint=m.group('to')
                ms=re.match(r"(?P<main>\S+\d+)(?P<sub>[A-Z])",fromfootprint)
                mt=re.match(r"(?P<main>\S+\d+)(?P<sub>[A-Z])",tofootprint)
                if fromfootprint in srcfpdict:
                    srcfp=srcfpdict[fromfootprint]
                    dstfpdict[tofootprint]=srcfp
                    srcfp.SetReference(tofootprint)
                elif (netfootprint=="GND"):
                    pass
                elif ms and mt:
                    if (ms.group('main') in footprintdict):
                        footprintdict[ms.group('main')].SetReference(mt.group('main'))
                    else:
                        print('not in sub name refs',fromfootprint,ms.group('main'))
                else:
                    print('not in refs %s'%fromfootprint)
            elif (l=='Annotation complete.'):
                pass
            else:
                print('not match',l)
    b.Save('%s.kicad_pcb'%proj)

    #
    #
    #            if (fromfootprint in footprintdict):
    #                footprintdict[fromfootprint].SetReference(tofootprint)
    #            elif (netfootprint=="GND"):
    #                pass
    #            elif ms and mt:
    #                if (ms.group('main') in footprintdict):
    #                    footprintdict[ms.group('main')].SetReference(mt.group('main'))
    #                else:
    #                    print('not in sub name refs',fromfootprint,ms.group('main'))
    #            else:
    #                print('not in refs',fromfootprint)
    #        else:
    #            print('not match',l)
    #
    #board.Save('test_refswap.kicad_pcb')
    #
