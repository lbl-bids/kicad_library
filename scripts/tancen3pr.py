from matplotlib import pyplot
from numpy import *
def maxtan(v1,v2,v3,v4):
    if (angle(v2-v1)==angle(v4-v3)) and (angle(v2-v1)!=angle(v3-v2)):
        v21=v2-v1
        nv21=exp(1j*angle(v21))
        nv1=(v1-v2)/nv21
        nv2=(v2-v2)/nv21
        nv3=(v3-v2)/nv21
        nv4=(v4-v2)/nv21
        ang1=angle(nv1)%(2*pi)
        ang3=angle(nv3)%(2*pi)
        angc=(ang1+ang3)/2
        d23=abs(nv2-nv3)
        xmaxtan=d23/2
        rmaxcal=abs(xmaxtan*tan(angc))
        rmax=min(rmaxcal,abs(v2-v1),abs(v4-v3))
    else:
        rmax=None
    return rmax

def tancen3pr(v1,v2,v3,r,plot=False):
    v21=v2-v1
    nv21=exp(1j*angle(v21))
    nv1=(v1-v2)/nv21
    nv2=(v2-v2)/nv21
    nv3=(v3-v2)/nv21
    ang1=angle(nv1)%(2*pi)
    ang3=angle(nv3)%(2*pi)
    angc=(ang1+ang3)/2
    dc=abs(r/sin(angc))
    nvc=dc*exp(1j*angc)
    npt1=abs(dc*cos(angc))*exp(1j*ang1)
    npt2=nvc-r*exp(1j*angc)
    npt3=abs(dc*cos(angc))*exp(1j*ang3)
    vc=nvc*nv21+v2
    pt1=npt1*nv21+v2
    pt2=npt2*nv21+v2
    pt3=npt3*nv21+v2
    if plot:
        pyplot.plot(v1.real,v1.imag,'r*')
        pyplot.plot(v2.real,v2.imag,'g*')
        pyplot.plot(v3.real,v3.imag,'b*')
        pyplot.plot(vc.real,vc.imag,'k*')
        pyplot.plot(pt1.real,pt1.imag,'y*')
        pyplot.plot(pt2.real,pt2.imag,'y*')
        pyplot.plot(pt3.real,pt3.imag,'y*')
        pp=vc+r*exp(1j*linspace(0,2*pi,50))
        pyplot.plot(pp.real,pp.imag,'k')
        pyplot.show()
    return (vc,pt1,pt2,pt3)
def roffset(vc,pt1,pt2,pt3,r,plot=False):
    npt1=vc+r*exp(1j*angle(pt1-vc))
    npt2=vc+r*exp(1j*angle(pt2-vc))
    npt3=vc+r*exp(1j*angle(pt3-vc))
    if plot:
        pyplot.plot(vc.real,vc.imag,'b*')
        pyplot.plot(pt1.real,pt1.imag,'r*')
        pyplot.plot(pt2.real,pt2.imag,'r*')
        pyplot.plot(pt3.real,pt3.imag,'r*')
        pyplot.plot(npt1.real,npt1.imag,'g*')
        pyplot.plot(npt2.real,npt2.imag,'g*')
        pyplot.plot(npt3.real,npt3.imag,'g*')
        pyplot.show()
    return (vc,npt1,npt2,npt3)

if __name__=="__main__":
    x0=50;y0=20
    x1=100;y1=20
    x2=200;y2=-153
    x3=400;y3=-153
    v0=x0+y0*1j
    v1=x1+y1*1j
    v2=x2+y2*1j
    v3=x3+y3*1j
    vc,pt1,pt2,pt3=tancen3pr(v0,v1,v2,173.15,plot=True)
    print(vc,pt1,pt2,pt3)
    print(roffset(vc,pt1,pt2,pt3,173.15,plot=True))
    tancen3pr(v1,v2,v3,100,plot=True)
    print(maxtan(v0,v1,v2,v3))

