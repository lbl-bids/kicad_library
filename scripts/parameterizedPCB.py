import pcbnew
import os
def parameterizepcb(projname, replist,digiparts):
    print(digiparts)
    b=pcbnew.LoadBoard("%s.kicad_pcb"%projname)
    footprints=b.GetFootprints()
    fpdict={i.GetReference():i for i in footprints}

    for rname,replace in replist:
        #replace = locals()[rname]
        for fps,props in replace.items():
            for fp in fps.split('|'):
                for k,v in props.items():
                    if k=="Value":
                        value=str(props['Value'])
                        if value=='DNP':
                            fpdict[fp].SetField('Value',value)
                        else:
                            if value in digiparts:
                                fpdict[fp].SetField('Value',value)
                                fpdict[fp].SetField('Vendor','digikey:%s'%digiparts[value])
                            else:
                                print('value not in digiparts',value)
                        fields=fpdict[fp].GetFields()
                        fielddict={f.GetName():f for f in fields}
                        for fname in ['Vendor','manufacturer','manufacturer_part_number']:
                            if fname in fielddict:
                                fielddict[fname].SetVisible(False)
                    else:
                        fpdict[fp].SetField(k,v)


                        print('%s replace %s was %s prop %s'%(rname,fp,fpdict[fp].GetValue(),str(props)))
        b.Save('%s_%s.kicad_pcb'%(projname,rname))
        # symlink the schematic, when open project, please always update schematic from layout
        schname='%s_%s.kicad_sch'%(projname,rname)
        srcsch='%s.kicad_sch'%projname
        schexist=False
        schcorrect=False
        if os.path.exists(schname):
            if os.path.islink(schname):
                if os.readlink(schname)==srcsch:
                    schexist=True
                    schcorrect=True
                    pass
                else:
                    schexist=True
            else:
                schexist=True
        if not schexist:
            os.symlink('%s.kicad_sch'%projname,'%s_%s.kicad_sch'%(projname,rname))
        else:
            if not schcorrect:
                print('Error, %s exist and not point to the schmatic I want'%schname)

if __name__=="__main__":
    import json
    digiparts={
            }
    #replist=[('NOAMP',{( "J2|J3|J4|J5"):{"Value":"DNP"}})
    #]
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-json',dest='jsonfile',help='json file (json)',default='replace.json')
    parser.add_argument(dest='projname',help='original kicad_pcb file basename, shared with kicad_sch')
    clargs=parser.parse_args()


    with open(clargs.jsonfile) as jfile:
        jdict=json.load(jfile)
    projname=clargs.projname #'balunX4'
    parameterizepcb(projname=projname,replist=jdict['replist'],digiparts=jdict['digiparts'])
