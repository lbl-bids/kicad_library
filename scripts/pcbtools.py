import pcbnew
import json
import oneboard
import numpy
# breadkout vscore: null mouse bite: -1 square open, -2 within boundary, -3 beyound boundary
def uuidinc(c,uuidincref):
    if uuidincref is not None:
        if isinstance(uuidincref,pcbnew.KIID):
            uuidincref.Increment()
            c.m_Uuid.Clone(uuidincref)
        else:
            print('uuidincref need to be a valid pcbnew KIID')
    else:
        pass
def pcbtext(board,x,y,text,size=pcbnew.VECTOR2I(1000000,2000000),uuidincref=None):
    c=pcbnew.PCB_TEXT(board)
    uuidinc(c,uuidincref)
    c.SetPos(pcbnew.VECTOR2I(int(round(x,10)),int(round(y,10))))
    c.SetHorizJustify(pcbnew.GR_TEXT_H_ALIGN_CENTER)
    c.SetLayer(pcbnew.Edge_Cuts)
    c.SetText(text)
    c.SetTextSize(size)
    board.Add(c)
def edgecutscircle(board,center,radius,uuidincref=None):
    c=pcbnew.PCB_SHAPE(board)
    uuidinc(c,uuidincref)
    c.SetShape(pcbnew.SHAPE_T_CIRCLE)
    c.SetCenter(pcbnew.VECTOR2I(center))
    c.SetLayer(pcbnew.Edge_Cuts)
    c.SetRadius(radius)
    board.Add(c)

def edgecutsarc(board,center,start,end,width,uuidincref=None):
    c=pcbnew.PCB_SHAPE(board)
    uuidinc(c,uuidincref)
    c.SetShape(pcbnew.SHAPE_T_ARC)
    c.SetCenter(pcbnew.VECTOR2I(center))
    c.SetStart(pcbnew.VECTOR2I(start))
    c.SetEnd(pcbnew.VECTOR2I(end))
    c.SetLayer(pcbnew.Edge_Cuts)
    c.SetWidth(int(round(width,10)))
    board.Add(c)
def edgecutssegment(board,start,end,width,uuidincref=None):
    c=pcbnew.PCB_SHAPE(board)
    uuidinc(c,uuidincref)
    c.SetShape(pcbnew.SHAPE_T_SEGMENT)
    c.SetStart(pcbnew.VECTOR2I(start))
    c.SetEnd(pcbnew.VECTOR2I(end))
    c.SetLayer(pcbnew.Edge_Cuts)
    c.SetWidth(int(round(width,10)))
    board.Add(c)
def edgecutsrect(board,start,end,width,uuidincref=None):
    c=pcbnew.PCB_SHAPE(board)
    uuidinc(c,uuidincref)
    c.SetShape(pcbnew.SHAPE_T_RECT)
    c.SetStart(pcbnew.VECTOR2I(start))
    c.SetEnd(pcbnew.VECTOR2I(end))
    c.SetLayer(pcbnew.Edge_Cuts)
    c.SetWidth(int(round(width,10)))
    board.Add(c)
if __name__=="__main__":
    edgeonly=0
    ofilename='panel.kicad_pcb'
    with open('panel.json') as jfile:
        panelcfg=json.load(jfile)
    
    b1=pcbnew.NewBoard(ofilename)
    copperlayercount=6
    b1.SetCopperLayerCount(copperlayercount)

    llx=0;lly=0
    urx=0;ury=0;
    llbx=0;llby=0;
    urbx=0;urby=0;
    lastref=None
    boardcorner={}
    halfvscore=300000
    halfmousebite=1500000
    panelxmax=int(round(16*25.4*1000000,10))
    panelymax=int(round(22*25.4*1000000,10))
    edgecutsrect(b1,pcbnew.VECTOR2I(0,0),pcbnew.VECTOR2I(panelxmax,-panelymax),100000)
    hlinelist=[]
    vlinelist=[]
    for ib,bcfg in enumerate(panelcfg):
        mousebitedict=dict(left={},bottom={},right={},top={})
        bid,b=list(bcfg.items())[0]
        print(bid)
        if 'netprefix' in b:
            netprefix=b['netprefix']
        else:
            netprefix='_%s'%bid
        if 'rotorigin' in b:
            rotorigin=pcbnew.VECTOR2I(b['rotorigin'][0],b['rotorigin'][1])
        else:
            rotorigin=pcbnew.VECTOR2I(0,0)
        if 'rotdeg' in b:
            rotdeg=pcbnew.EDA_ANGLE(b['rotdeg'],pcbnew.DEGREES_T)
        else:
            rotdeg=pcbnew.EDA_ANGLE(0,pcbnew.DEGREES_T)
        if 'refboard' in b:
            refx,refy=b['refboard']
        else:
            refx,refy=(lastref,lastref)
        if 'lowerleft' in b:
            lowerleft=pcbnew.VECTOR2I(b['lowerleft'][0],b['lowerleft'][1])
        elif 'deltalowerleftx' in b:
            lowerleft=pcbnew.VECTOR2I(boardcorner[refx]['ur'].x+b['deltalowerleftx'],boardcorner[refy]['ll'].y)
        elif 'deltalowerlefty' in b:
            lowerleft=pcbnew.VECTOR2I(boardcorner[refx]['ll'].x,boardcorner[refy]['ur'].y-b['deltalowerlefty'])
        elif 'deltalowerleft' in b:
            lowerleft=pcbnew.VECTOR2I(boardcorner[refx]['ur'].x+b['deltalowerleft'][0],boardcorner[refy]['ur'].y-b['deltalowerleft'][1])
        elif 'xyoffset' in b:
            idx,idy=b['xyoffset']

            lowerleftx=boardcorner[refx]['llbreakout'].x if idx==0 else boardcorner[refx]['urbreakout'].x
            lowerlefty=boardcorner[refy]['llbreakout'].y if idy==0 else boardcorner[refy]['urbreakout'].y
            lowerleft=pcbnew.VECTOR2I(lowerleftx,lowerlefty)
        else:
            print('on else?')
            lowerleft=pcbnew.VECTOR2I(0,0)
        if 'breakout' in b:
            [left,bottom,right,top]=b['breakout']
            if left is None: # NA
                llbx=lowerleft.x
                llx=lowerleft.x
            elif isinstance(left,list): # mouse bite
                llbx=lowerleft.x
                llx=llbx+halfmousebite
                mousebitedict['left'].update(dict(x=llbx,y=left))
                #if len(left)<3:
                #    exit('need at least 3 item to specify starting location and ending')
                #else:
                #    if left[0]==-1: # connect to another mouse bit, so just square to center, no close
                #        llby=lly-halfmousebite
                #        inst1p.append(['line',llb])
                #        inst2p.append(['line'])
                #    elif left[0]==-2: # not connect to anything, close by arc
                #        llby=lly-halfmousebite
                #        outlineinstruction.append([llb,'arc'])
            elif left==0: # vscore
                llbx=lowerleft.x
                llx=llbx+halfvscore
                vlinelist.append(llbx)
            else:
                exit('left %s'%str(left))
            if bottom is None:
                llby=lowerleft.y
                lly=lowerleft.y
            elif isinstance(bottom,list):
                llby=lowerleft.y
                lly=llby-halfmousebite
                mousebitedict['bottom'].update(dict(x=bottom,y=llby))
            elif bottom==0:
                llby=lowerleft.y
                lly=llby-halfvscore
                hlinelist.append(llby)
            else:
                exit('bottom %s'%str(bottom))
            lowerleft=pcbnew.VECTOR2I(llx,lly)
            
        ll,ur=oneboard.oneboard(newboard=b1
,netprefix=netprefix
,filename=b['filename']
,rotorigin=rotorigin
,rotdeg=rotdeg
,lowerleft=lowerleft
,edgeonly=edgeonly
)
        llx=ll.x
        lly=ll.y
        urx=ur.x
        ury=ur.y
        if 'breakout' in b:
            if top is None:
                urby=ury
            elif isinstance(top,list):
                urby=ury-halfmousebite
                mousebitedict['top'].update(dict(x=top,y=urby))
            elif top==0:
                urby=ury-halfvscore
                hlinelist.append(urby)
            else:
                exit('top %s'%str(top))
            if right is None:
                urbx=urx
            elif isinstance(right,list):
                urbx=urx+halfmousebite
                mousebitedict['right'].update(dict(x=urbx,y=right))
            elif right==0:
                urbx=urx+halfvscore
                vlinelist.append(urbx)
            else:
                exit('right %s'%str(right))
            boardcorner[bid]=dict(ll=pcbnew.VECTOR2I(llx,lly),ur=pcbnew.VECTOR2I(urx,ury),urbreakout=pcbnew.VECTOR2I(urbx,urby),llbreakout=pcbnew.VECTOR2I(llbx,llby))
        else:
            print('update bid 1',bid)
            boardcorner[bid]=dict(ll=pcbnew.VECTOR2I(llx,lly),ur=pcbnew.VECTOR2I(urx,ury),urbreakout=pcbnew.VECTOR2I(urx,ury),llbreakout=pcbnew.VECTOR2I(llx,lly))
        print(mousebitedict)

        lastref=bid
        for eddir in ['left','bottom','right','top']:
            dic=mousebitedict[eddir]
            if dic:
                mousebite(board=b1,x=dic['x'],y=dic['y'],boardcorner=boardcorner[bid],halfmousebite=halfmousebite)
        if edgeonly:
            pcbtext(board=b1,x=(llx+urx)/2,y=(lly+ury)/2,text='%s\n%s'%(bid,b['filename']),size=pcbnew.VECTOR2I(1000000,2000000))
    vline=sorted(set(vlinelist))
    hline=sorted(set(hlinelist))
    for v in vline:
        edgecutssegment(b1,pcbnew.VECTOR2I(v,0),pcbnew.VECTOR2I(v,-panelymax),100000)
    for h in hline:
        edgecutssegment(b1,pcbnew.VECTOR2I(0,h),pcbnew.VECTOR2I(panelxmax,h),100000)


    b1.Save(ofilename)

'''
    ifilename="slot/slot.kicad_pcb"
    netprefix='_b1'
    #netprefix=''
    rotorigin=pcbnew.VECTOR2I(0,0)
    rotdeg=pcbnew.EDA_ANGLE(0,pcbnew.DEGREES_T)
    lowerleft=pcbnew.VECTOR2I(0,0)
    ur=oneboard(newboard=b1
,netprefix='_b1'
,filename='../diffcarrier/diffcarrier.kicad_pcb'
,rotorigin=rotorigin
,rotdeg=rotdeg
,lowerleft=lowerleft
,edgeonly=0
)
    ur=oneboard(newboard=b1
,netprefix='_b2'
,filename=ifilename
,rotorigin=rotorigin
,rotdeg=rotdeg
,lowerleft=pcbnew.VECTOR2I(urx+3000000,lowerleft.y)
,edgeonly=0)
'''
