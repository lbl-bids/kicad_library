import pcbnew
import json
import oneboard
import numpy
from pcbtools import *
def mousebite(board,x,y,boardcorner,halfmousebite ,holes=5,drill=20,size=30,width=200000,nbiterow=2,nbiteperrow=5,biteradius=20*25400,bitecenterspace=70*25400,uuidincref=None,debug=False):
#def mousebite(board,x,y,boardcorner,halfmousebite ,holes=5,drill=20,size=30,width=200000,nbite=5,biteradius=5*25400,bitecenterspace=20*25400):
    mblen=(nbiteperrow+1)*bitecenterspace+biteradius+2*halfmousebite
    halfmblen=mblen/2.0
    # ending code: -1 close within boundary, -2 open, -3 close by extend
    if isinstance(x,list) and isinstance(y,int):
        xd=boardcorner['urbreakout'].x-boardcorner['llbreakout'].x
        ending=[x[0],x[-1]]
        x[0]=0
        x[-1]=xd
        x=boardcorner['llbreakout'].x+numpy.array(x)
        if debug:
            print(x,boardcorner)
        for ix,xi in enumerate(x[0:-1]): 
            delta0= 0 if ix==0 else mblen/2#mblen/2 if ix%2 else mblen/2
            delta1= 0 if ix==len(x)-2 else -mblen/2#-mblen/2 if ix%2 else -mblen/2

            #p0=pcbnew.VECTOR2I(int(xi+delta0),int(y+halfmousebite))
            #p1=pcbnew.VECTOR2I(int(xi+delta0),int(y-halfmousebite))
            #pc0=pcbnew.VECTOR2I(int(xi+delta0),int(y))
            #p2=pcbnew.VECTOR2I(int(x[ix+1]+delta1),int(y+halfmousebite))
            #p3=pcbnew.VECTOR2I(int(x[ix+1]+delta1),int(y-halfmousebite))
            #pc1=pcbnew.VECTOR2I(int(x[ix+1]+delta1),int(y))
            

            #edgecutsarc(board=board,center=pc0 if ix%2 else pc1 ,start=p0 if ix%2 else p3,end=p1 if ix%2 else p2,width=width)
            if ix==0: # the xi are at the breakout location
                p0=pcbnew.VECTOR2I(int(xi),int(y+halfmousebite))
                p1=pcbnew.VECTOR2I(int(xi),int(y-halfmousebite))
                pc01=pcbnew.VECTOR2I(int(xi),int(y))
                p01exn=pcbnew.VECTOR2I(int(xi-halfmousebite),int(y))
                p2=pcbnew.VECTOR2I(int(xi+halfmousebite),int(y+halfmousebite))
                p3=pcbnew.VECTOR2I(int(xi+halfmousebite),int(y-halfmousebite))
                pc23=pcbnew.VECTOR2I(int(xi+halfmousebite),int(y))
                p4=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y-halfmousebite))
                p5=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y+halfmousebite))
                pc45=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y))
                p6=pcbnew.VECTOR2I(int(xi-halfmousebite),int(y+halfmousebite))
                p7=pcbnew.VECTOR2I(int(xi-halfmousebite),int(y-halfmousebite))
                pc67=pcbnew.VECTOR2I(int(xi-halfmousebite),int(y))
                edgecutssegment(board=board,start=p2,end=p5,width=width,uuidincref=uuidincref)
                edgecutssegment(board=board,start=p3,end=p4,width=width,uuidincref=uuidincref)
                edgecutsarc(board=board,center=pc45,start=p4 ,end=p5,width=width,uuidincref=uuidincref)
                if ending[0] == -1:
                    pass
                elif ending[0] == -2:
                    edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                elif ending[0] == -3:
                    edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                elif ending[0] == -4:
                    edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                elif ending[0] == -5:
                    edgecutsarc(board=board,center=pc23,start=p2,end=p3,width=width,uuidincref=uuidincref)
                elif ending[0] == -6:
                    edgecutsarc(board=board,center=pc01,start=p0,end=p1,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                elif ending[0] == -7:
                    edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                    edgecutsarc(board=board,center=pc01,start=p0,end=p01exn,width=width,uuidincref=uuidincref)
                elif ending[0] == -8:
                    edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                    edgecutsarc(board=board,center=pc01,start=p01exn,end=p01,width=width,uuidincref=uuidincref)
                elif ending[0] == -9:
                    edgecutsarc(board=board,center=pc67,start=p6,end=p7,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p6,end=p2,width=width,uuidincref=uuidincref)
                elif ending[0] == -10:
                    edgecutsarc(board=board,center=pc67,start=p6,end=p7,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p7,end=p3,width=width,uuidincref=uuidincref)

                    pass
                #edgecutsarc(board=board,center=pc1,start=p3 ,end=p2,width=width)
            elif ix==len(x)-2:
                p0=pcbnew.VECTOR2I(int(xi+halfmblen),int(y+halfmousebite))
                p1=pcbnew.VECTOR2I(int(xi+halfmblen),int(y-halfmousebite))
                pc01=pcbnew.VECTOR2I(int(xi+halfmblen),int(y))
                p2=pcbnew.VECTOR2I(int(x[ix+1]-halfmousebite),int(y-halfmousebite))
                p3=pcbnew.VECTOR2I(int(x[ix+1]-halfmousebite),int(y+halfmousebite))
                pc23=pcbnew.VECTOR2I(int(x[ix+1]-halfmousebite),int(y))
                p4=pcbnew.VECTOR2I(int(x[ix+1]),int(y-halfmousebite))
                p5=pcbnew.VECTOR2I(int(x[ix+1]),int(y+halfmousebite))
                pc45=pcbnew.VECTOR2I(int(x[ix+1]),int(y))
                p45exp=pcbnew.VECTOR2I(int(x[ix+1]+halfmousebite),int(y))
                p6=pcbnew.VECTOR2I(int(x[ix+1]+halfmousebite),int(y-halfmousebite))
                p7=pcbnew.VECTOR2I(int(x[ix+1]+halfmousebite),int(y+halfmousebite))
                pc67=pcbnew.VECTOR2I(int(x[ix+1]+halfmousebite),int(y))
                edgecutssegment(board=board,start=p1,end=p2,width=width,uuidincref=uuidincref)
                edgecutssegment(board=board,start=p0,end=p3,width=width,uuidincref=uuidincref)
                edgecutsarc(board=board,center=pc01,start=p0 ,end=p1,width=width,uuidincref=uuidincref)
                if ending[1] == -1:
                    pass
                elif ending[1] == -2:
                    edgecutssegment(board=board,start=p3,end=p5,width=width,uuidincref=uuidincref)
                elif ending[1] == -3:
                    edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                elif ending[1] == -4:
                    edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p3,end=p5,width=width,uuidincref=uuidincref)
                elif ending[1] == -5:
                    edgecutsarc(board=board,center=pc23,start=p2,end=p3,width=width,uuidincref=uuidincref)
                elif ending[1] == -6:
                    edgecutsarc(board=board,center=pc45,start=p4,end=p5,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p3,end=p5,width=width,uuidincref=uuidincref)
                elif ending[1] == -7:
                    edgecutssegment(board=board,start=p3,end=p5,width=width)
                    edgecutsarc(board=board,center=pc45,start=p45exp,end=p5,width=width,uuidincref=uuidincref)
                elif ending[1] == -8:
                    edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                    edgecutsarc(board=board,center=pc45,start=p4,end=p45exp,width=width,uuidincref=uuidincref)
                elif ending[1] == -9:
                    edgecutsarc(board=board,center=pc45,start=p4,end=p5,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p3,end=p7,width=width,uuidincref=uuidincref)
                elif ending[1] == -10:
                    edgecutsarc(board=board,center=pc45,start=p4,end=p5,width=width,uuidincref=uuidincref)
                    edgecutssegment(board=board,start=p2,end=p6,width=width,uuidincref=uuidincref)
                    pass
                #if ending[1] in [-1,-3]:
                #    print(bid,ending)
                #    edgecutsarc(board=board,center=pc1,start=p3 ,end=p2,width=width)
                #edgecutsarc(board=board,center=pc0,start=p0 ,end=p1,width=width)
                #    #edgecutsarc(board=board,center=pc1 if ix%2 else pc0,start=p3 if ix%2 else p0,end=p2 if ix%2 else p1,width=width)
                pass
            else:
                p0=pcbnew.VECTOR2I(int(xi+halfmblen),int(y+halfmousebite))
                p1=pcbnew.VECTOR2I(int(xi+halfmblen),int(y-halfmousebite))
                pc01=pcbnew.VECTOR2I(int(xi+halfmblen),int(y))
                p2=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y-halfmousebite))
                p3=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y+halfmousebite))
                pc23=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y))
                edgecutssegment(board=board,start=p0,end=p3,width=width,uuidincref=uuidincref)
                edgecutssegment(board=board,start=p1,end=p2,width=width,uuidincref=uuidincref)
                edgecutsarc(board=board,center=pc01,start=p0 ,end=p1,width=width,uuidincref=uuidincref)
                edgecutsarc(board=board,center=pc23,start=p2 ,end=p3,width=width,uuidincref=uuidincref)
                #edgecutsarc(board=board,center=pc1 if ix%2 else pc0 ,start=p3 if ix%2 else p0,end=p2 if ix%2 else p1,width=width)
                if debug:
                    print('xelse')
        for ix,xi in enumerate(x[1:-1]):
            for j in range(nbiterow):
                for i in range(nbiteperrow):
                    xbite=int(round(xi-(i-(nbiteperrow-1)/2)*bitecenterspace,10))
                    if nbiterow==1:
                        ybite=y
                    elif nbiterow==2:
                        if j==0:
                            ybite=int(round(y-halfmousebite+1.2*biteradius,10))
                        else:
                            ybite=int(round(y+halfmousebite-1.2*biteradius,10))
                    else:
                        exit('nbite row should be 1 or 2, not %s'%(str(nbiterow)))
                    edgecutscircle(board=board,center=pcbnew.VECTOR2I(xbite,ybite),radius=biteradius,uuidincref=uuidincref)

    elif isinstance(y,list) and isinstance(x,int):
        yd=boardcorner['urbreakout'].y-boardcorner['llbreakout'].y
        if len(y)>0:
            ending=[y[0],y[-1]]
            y[0]=0 #halfmousebite if ending[0]==-1 else 0
            y[-1]=-yd#-(halfmousebite if ending[1]==-1 else 0)
            y=boardcorner['llbreakout'].y-numpy.array(y)
            for iy,yi in enumerate(y[0:-1]):
                if 1:
                    if iy==0:
                        p0=pcbnew.VECTOR2I(int(x+halfmousebite),int(yi))
                        p1=pcbnew.VECTOR2I(int(x-halfmousebite),int(yi))
                        pc01=pcbnew.VECTOR2I(int(x),int(yi))
                        p01eyn=pcbnew.VECTOR2I(int(x),int(yi+halfmousebite))
                        p2=pcbnew.VECTOR2I(int(x+halfmousebite),int(yi-halfmousebite))
                        p3=pcbnew.VECTOR2I(int(x-halfmousebite),int(yi-halfmousebite))
                        pc23=pcbnew.VECTOR2I(int(x),int(yi-halfmousebite))
                        p5=pcbnew.VECTOR2I(int(x+halfmousebite),int(y[iy+1]+halfmblen))
                        p4=pcbnew.VECTOR2I(int(x-halfmousebite),int(y[iy+1]+halfmblen))
                        pc45=pcbnew.VECTOR2I(int(x),int(y[iy+1]+halfmblen))
                        p6=pcbnew.VECTOR2I(int(x+halfmousebite),int(yi+halfmousebite))
                        p7=pcbnew.VECTOR2I(int(x-halfmousebite),int(yi+halfmousebite))
                        pc67=pcbnew.VECTOR2I(int(x),int(yi+halfmousebite))
                        edgecutsarc(board=board,center=pc45,start=p4,end=p5,width=width,uuidincref=uuidincref)
                        edgecutssegment(board=board,start=p4,end=p3,width=width,uuidincref=uuidincref)
                        edgecutssegment(board=board,start=p5,end=p2,width=width,uuidincref=uuidincref)

                        if ending[0] == -1:
                            pass
                        elif ending[0] == -2:
                            edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                        elif ending[0] == -3:
                            edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                        elif ending[0] == -4:
                            edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                        elif ending[0] == -5:
                            edgecutsarc(board=board,center=pc23,start=p2,end=p3,width=width,uuidincref=uuidincref)
                        elif ending[0] == -6:
                            edgecutsarc(board=board,center=pc01,start=p0,end=p1,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                        elif ending[0] == -7:
                            edgecutssegment(board=board,start=p1,end=p3,width=width,uuidincref=uuidincref)
                            edgecutsarc(board=board,center=pc01,start=p01eyn,end=p1,width=width,uuidincref=uuidincref)
                        elif ending[0] == -8:
                            edgecutssegment(board=board,start=p0,end=p2,width=width,uuidincref=uuidincref)
                            edgecutsarc(board=board,center=pc01,start=p0,end=p01eyn,width=width,uuidincref=uuidincref)
                        elif ending[0] == -9:
                            edgecutsarc(board=board,center=pc67,start=p6,end=p7,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p7,end=p3,width=width,uuidincref=uuidincref)
                        elif ending[0] == -10:
                            edgecutsarc(board=board,center=pc67,start=p6,end=p7,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p6,end=p2,width=width,uuidincref=uuidincref)
                        pass
                        #if ending[0] in [-1,-3]:
                        #    edgecutsarc(board=board,center=pc0,start=p0 ,end=p1,width=width)
                        #edgecutsarc(board=board,center=pc1,start=p3 ,end=p2,width=width)
                    elif iy==len(y)-2:
                        p0=pcbnew.VECTOR2I(int(x+halfmousebite),int(yi-halfmblen))
                        p1=pcbnew.VECTOR2I(int(x-halfmousebite),int(yi-halfmblen))
                        pc01=pcbnew.VECTOR2I(int(x),int(yi-halfmblen))
                        p3=pcbnew.VECTOR2I(int(x+halfmousebite),int(y[iy+1]+halfmousebite))
                        p2=pcbnew.VECTOR2I(int(x-halfmousebite),int(y[iy+1]+halfmousebite))
                        pc23=pcbnew.VECTOR2I(int(x),int(y[iy+1]+halfmousebite))
                        p5=pcbnew.VECTOR2I(int(x+halfmousebite),int(y[iy+1]))
                        p4=pcbnew.VECTOR2I(int(x-halfmousebite),int(y[iy+1]))
                        pc45=pcbnew.VECTOR2I(int(x),int(y[iy+1]))
                        p7=pcbnew.VECTOR2I(int(x+halfmousebite),int(y[iy+1]-halfmousebite))
                        p6=pcbnew.VECTOR2I(int(x-halfmousebite),int(y[iy+1]-halfmousebite))
                        pc67=pcbnew.VECTOR2I(int(x),int(y[iy+1]-halfmousebite))
                        p45eyp=pcbnew.VECTOR2I(int(x),int(y[iy+1])-halfmousebite)
                        edgecutsarc(board=board,center=pc01,start=p0,end=p1,width=width,uuidincref=uuidincref)
                        edgecutssegment(board=board,start=p0,end=p3,width=width,uuidincref=uuidincref)
                        edgecutssegment(board=board,start=p1,end=p2,width=width,uuidincref=uuidincref)
                        if ending[1] == -1:
                            pass
                        elif ending[1] == -2:
                            edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                        elif ending[1] == -3:
                            edgecutssegment(board=board,start=p3,end=p5,width=width,uuidincref=uuidincref)
                        elif ending[1] == -4:
                            edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p3,end=p5,width=width,uuidincref=uuidincref)
                        elif ending[1] == -5:
                            edgecutsarc(board=board,center=pc23,start=p2,end=p3,width=width,uuidincref=uuidincref)
                        elif ending[1] == -6:
                            edgecutsarc(board=board,center=pc45,start=p4,end=p5,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p2,end=p4,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p3,end=p5,width=width,uuidincref=uuidincref)
                        elif ending[1] == -7:
                            edgecutssegment(board=board,start=p4,end=p2,width=width,uuidincref=uuidincref)
                            edgecutsarc(board=board,center=pc45,start=p4,end=p45eyp,width=width,uuidincref=uuidincref)
                        elif ending[1] == -8:
                            edgecutssegment(board=board,start=p5,end=p3,width=width,uuidincref=uuidincref)
                            edgecutsarc(board=board,center=pc45,start=p45eyp,end=p5,width=width,uuidincref=uuidincref)
                        elif ending[1] == -9:
                            edgecutsarc(board=board,center=pc67,start=p6,end=p7,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p2,end=p6,width=width,uuidincref=uuidincref)
                        elif ending[1] == -10:
                            edgecutsarc(board=board,center=pc67,start=p6,end=p7,width=width,uuidincref=uuidincref)
                            edgecutssegment(board=board,start=p3,end=p7,width=width,uuidincref=uuidincref)
                        pass
                        #if ending[1] in [-1,-3]:
                        #    #edgecutsarc(board=board,center=pc1 if iy%2 else pc0 ,start=p3 if iy%2 else p0,end=p2 if iy%2 else p1,width=width)
                        #    edgecutsarc(board=board,center=pc1,start=p3 ,end=p2,width=width)
                        #edgecutsarc(board=board,center=pc0,start=p0 ,end=p1,width=width)
                        #    #edgecutsarc(board=board,center=pc1,start=p3 ,end=p2,width=width)
                    else:
                        p0=pcbnew.VECTOR2I(int(x+halfmousebite),int(yi-halfmblen))
                        p1=pcbnew.VECTOR2I(int(x-halfmousebite),int(yi-halfmblen))
                        pc01=pcbnew.VECTOR2I(int(x),int(yi-halfmblen))
                        p3=pcbnew.VECTOR2I(int(x+halfmousebite),int(y[iy+1]+halfmblen))
                        p2=pcbnew.VECTOR2I(int(x-halfmousebite),int(y[iy+1]+halfmblen))
                        pc23=pcbnew.VECTOR2I(int(x),int(y[iy+1]+halfmblen))
                        edgecutssegment(board=board,start=p0,end=p3,width=width,uuidincref=uuidincref)
                        edgecutssegment(board=board,start=p1,end=p2,width=width,uuidincref=uuidincref)
                        edgecutsarc(board=board,center=pc01,start=p0,end=p1,width=width,uuidincref=uuidincref)
                        edgecutsarc(board=board,center=pc23,start=p2,end=p3,width=width,uuidincref=uuidincref)
                    #p0=pcbnew.VECTOR2I(int(xi+halfmblen),int(y+halfmousebite))
                    #p1=pcbnew.VECTOR2I(int(xi+halfmblen),int(y-halfmousebite))
                    #pc01=pcbnew.VECTOR2I(int(xi+halfmblen),int(y))
                    #p2=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y-halfmousebite))
                    #p3=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y+halfmousebite))
                    #pc23=pcbnew.VECTOR2I(int(x[ix+1]-halfmblen),int(y))
                    #edgecutssegment(board=board,start=p0,end=p3,width=width)
                    #edgecutssegment(board=board,start=p1,end=p2,width=width)
                    #edgecutsarc(board=board,center=pc01,start=p0 ,end=p1,width=width)
                    #edgecutsarc(board=board,center=pc23,start=p2 ,end=p3,width=width)
                        #edgecutsarc(board=board,center=pc0,start=p0 ,end=p1,width=width)
                        #edgecutsarc(board=board,center=pc1,start=p3 ,end=p2,width=width)
                        #edgecutsarc(board=board,center=pc1 if iy%2 else pc0 ,start=p3 if iy%2 else p0,end=p2 if iy%2 else p1,width=width)
                        #edgecutsarc(board=board,center=pc1,start=p3,end=p2,width=width)
            for iy,yi in enumerate(y[1:-1]):
                for j in range(nbiterow):
                    for i in range(nbiteperrow):
                        if nbiterow==1:
                            xbite=x
                        elif nbiterow==2:
                            if j==0:
                                xbite=int(round(x-halfmousebite+1.2*biteradius,10))
                            else:
                                xbite=int(round(x+halfmousebite-1.2*biteradius,10))
                        else:
                            exit('nbite row should be 1 or 2, not %s'%(str(nbiterow)))
                        ybite=int(round(yi-(i-(nbiteperrow-1)/2)*bitecenterspace,10))
                        edgecutscircle(board=board,center=pcbnew.VECTOR2I(xbite,ybite),radius=biteradius,uuidincref=uuidincref)

    else:
        exit('what?')
    

if __name__=="__main__":
    pass
