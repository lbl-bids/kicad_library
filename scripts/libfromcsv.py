import re
import pprint
import pyparsing
import platform
import os
import re
import pprint
import json
import sys
from kicad_symbol_lib import c_lib
import argparse
if __name__=="__main__":
	parser=argparse.ArgumentParser()
	parser.add_argument('-g','--global',help='global sym lib table',dest='globallib',type=str,default='$HOME/.config/kicad/sym-lib-table')
	parser.add_argument('-l','--local',help='local sym lib table',dest='locallib',type=str,default='')
	parser.add_argument('-i','--inputtsv',help='derived symbol description tsv',dest='inputtsv',type=str,required=True)
	parser.add_argument('-o','--outputlib',help='output lib/dcm filename',dest='outputlib',type=str,default='')
	clargs=parser.parse_args()
	outputlib= clargs.inputtsv if clargs.outputlib=='' else clargs.outputlib



	system=platform.system()
	if system=='Linux':
		defaultcfg='$HOME/.config/kicad/kicad_common'
		#commonsym='$HOME/.config/kicad/sym-lib-table'
		commonsym=clargs.globallib
	elif system=='Windows':
		defaultcfg='???'
		commonsym='$HOME/.config/kicad/sym-lib-table'
	if os.path.isfile(os.path.expandvars(defaultcfg)):
		f=open(os.path.expandvars(defaultcfg))
		s=f.read()
		f.close()
		#print s
		menv=re.findall('\[EnvironmentVariables\]\s*(?P<menvs>[\s\S]*)',s,re.M)
		if menv:
			envs=re.findall('([\s\S]+?)=([\s\S]+?\s*)\n',menv[0],re.M)
			#print 'envs',envs
			for envname,envval in envs:
				os.environ[envname]=envval
			#pprint.pprint(os.environ)
#f=open('sym-lib-table')
#f=open('/home/ghuang/.config/kicad/sym-lib-table')
#f=open(clargs.globallib)
	libdict={}
	for symlibtable in [commonsym,clargs.locallib]:
		if symlibtable:
			f=open(os.path.expandvars(symlibtable))
			s=f.read()
			f.close()
#c=re.compile(r'\((\S+)\s+([\s\S]+?)\)')
#print c.match(s).groups()
			data = "( (a ( ( c ) b ) ) ( d ) e )"
#par=nestedExpr().parseString(s)#.asList()
			par=pyparsing.nestedExpr(opener='(',closer=')',content=None,ignoreExpr=pyparsing.quotedString).parseString(s)
#nestedExpr(opener, closer, content=None, ignoreExpr=quotedString)
#print par

			for tab in par:
				if tab[0]=='sym_lib_table':
					for lib in tab[1:]:
						if lib[0]=='lib':
							newdict={}
							for libdictstr in lib[1:]:
								newdict[libdictstr[0]]=libdictstr[1]
							libdict[newdict['name']]=newdict
								#print libdict
				#print lib
#	print tab
	allsyms={}
	for lib in libdict.keys():
		path=os.path.splitext(os.path.expandvars(libdict[lib]['uri']))[0]
		print path,os.path.isfile(path+'.lib')
		newlib=c_lib()
		newlib.libfromfile(path)
		#print newlib.getcompname()
		for compname in newlib.getcompname():
			key='%s:%s'%(lib,compname)
			if key in allsyms.keys():
				print 'duplicate comps:',key
			allsyms[key]=newlib.getsymbol(compname)

	import csv
	nlib=c_lib()
	fname=clargs.inputtsv#sys.argv[1]
	with open(fname+'.tsv') as tsvfile:
		reader=csv.DictReader(tsvfile,delimiter='\t')
		for row in reader:
			key='%s:%s'%(row['baselib'],row['basesym'])
			if key in allsyms.keys():
				#			print key,row['MFR'],row['model'],row['pinmap']
				basesymbol=allsyms[key]
				newsymbol=basesymbol.copy()
				MFR=row['MFR'] if 'MFR' in row.keys() and row['MFR'] else ''
				model=row['model'] if 'model' in row.keys() and row['model'] else ''
				pinmap=row['pinmap'] if 'pinmap' in row.keys() and row['pinmap'] else ''
				for k in [kk for kk in row.keys() if kk not in ['baselib','basesym','pinmap']]:
					newsymbol.addFline(field_name=k,text=row[k])

				if pinmap:
					pinmapdict=json.loads(row['pinmap'])
					newsymbol.mappins(pinmapdict)
				newsymbol.setname('%s_%s_%s'%(MFR,model,row['basesym']))
				print 'key',key
				nlib.addsymbol(newsymbol)
		nlib.updatecompname()
		print nlib.getcompname()
		(lib,dcm)=nlib.strgen()
		fwrite=outputlib
		flib=open(fwrite+'.lib','w')
		flib.write(lib)
		flib.close()
		fdcm=open(fwrite+'.dcm','w')
		fdcm.write(dcm)
		fdcm.close()

#pprint.pprint(libdict)

#[['sym_lib_table', ['lib', ['name', 't2'], ['type', 'Legacy'], ['uri', 'C:/temp/t2.lib'], ['options', '""'], ['descr', '""']]]]
