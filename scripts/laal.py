import pcbnew
import numpy
mil=25.4*1000
def comp2vec(c):
    return pcbnew.VECTOR2I(int(round(c.real,10)),int(round(c.imag,10)))
def addgroup(board,items):
    v=pcbnew.PCB_GROUP(board)
    for i in items:
        v.AddItem(i)
    board.Add(v)
    return v

def via(board,pos,size,drill):
    v=pcbnew.PCB_VIA(board)
    v.SetStart(pcbnew.VECTOR2I(pos))
    v.SetWidth(int(round(size,10)))
    v.SetDrill(int(round(drill,10)))
    board.Add(v)
    return v
#    pcbnew.Refresh()
def arc(board,start,mid,end,width,layer=0):
    a=pcbnew.PCB_ARC(board)
    board.Add(a)
    a.SetStart(pcbnew.VECTOR2I(start))
    a.SetEnd(pcbnew.VECTOR2I(end))
    a.SetMid(pcbnew.VECTOR2I(mid))
    a.SetWidth(int(round(width)))
    a.SetLayer(layer)
#    pcbnew.Refresh()
    return a
def track(board,start,end,width,layer=0):
    t=pcbnew.PCB_TRACK(board)
    board.Add(t)
    t.SetStart(pcbnew.VECTOR2I(start))
    t.SetEnd(pcbnew.VECTOR2I(end))
    t.SetWidth(int(round(width)))
    t.SetLayer(layer)
#    pcbnew.Refresh()
    return t

def x9y(p):
    return p.x*1000000000+p.y
def x9yminmax(p1,p2):
    x9yp1=x9y(p1)
    x9yp2=x9y(p2)
    (minval,maxval) = (p1,p2) if x9yp1<x9yp2 else (p2,p1)
    return (minval,maxval)
def laal(distance=35,offset=(9/2+6.5+7+21/2+2),unit=mil,removeavrtrack=True,laal=True,removenetvia=True,r=200,dr=9,width=6.5):
    r=r*unit
    dr=(dr+width)/2*unit
    width=width*unit
    b=pcbnew.GetBoard()
    tracks=b.GetTracks()
    selectednet={}
    for i,t in enumerate(tracks):
        if (t.IsSelected()):
            net=t.GetNetname()
            if net not in selectednet:
                selectednet[net]=[]
            selectednet[net].append(t)
            #print(i,t,t.GetStart(),t.GetEnd())
    if len(selectednet)==1:
        pass
    elif len(selectednet)==2:
        print(selectednet.keys())
    else:
        exit('select only 1(single ended) or 2 (diffential ended with P/N,+/-) nets')
    #print(selectednet)
    for net,nettrack in selectednet.items():
        nettracksorted=sorted(nettrack,key=lambda t:min(x9y(t.GetStart()),x9y(t.GetEnd())))
        selectednet[net]=nettracksorted
    nets=list(selectednet.keys())
    if len(selectednet)==2:
        avrtrack=[]
        avrorigin=[]
        for itrack,track0 in enumerate(selectednet[nets[0]]):
            track1=selectednet[nets[1]][itrack]
            layer=track0.GetLayer()
            print(track1.GetClass(),track0.GetClass())
            if track0.GetClass()=='PCB_TRACK':
                start0,end0=x9yminmax(track0.GetStart(),track0.GetEnd())
                start1,end1=x9yminmax(track1.GetStart(),track1.GetEnd())
                avrstart=(start0+start1)/2
                avrend=(end0+end1)/2
                t=track(board=b,start=avrstart,end=avrend,width=width,layer=layer)
                avrtrack.append(t)
                avrorigin.append((track0,track1))
            elif track0.GetClass()=='PCB_ARC':
                start0,end0=x9yminmax(track0.GetStart(),track0.GetEnd())
                start1,end1=x9yminmax(track1.GetStart(),track1.GetEnd())
                center0=track0.GetCenter()
                radius0=track0.GetRadius()
                center1=track1.GetCenter()
                radius1=track1.GetRadius()
                avrstart=(start0+start1)/2
                avrend=(end0+end1)/2
                avrcenter=(center0+center1)/2
                avrradius=(radius0+radius1)/2
                startvector=avrstart-avrcenter
                endvector=avrend-avrcenter
                startangle=numpy.arctan2(startvector.x,startvector.y)
                endangle=numpy.arctan2(endvector.x,endvector.y)
                midangle=(startangle+endangle)/2
                midxy=avrcenter+pcbnew.VECTOR2I(int(round(avrradius*numpy.cos(midangle))),int(round(avrradius*numpy.sin(midangle))))
                #track(board=b,start=avrstart,end=avrend,width=2*25.4*1000)
                a=arc(board=b,start=avrstart,mid=midxy,end=avrend,width=width)
                avrorigin.append((track0,track1))
                avrtrack.append(a)
                #print('start0',start0,'end0',end0,'start1',start1,'end1',end1,avrstart,avrend)

        if laal:
            from importlib import reload
            import tancen3pr
            reload(tancen3pr)
            ntrack=len(avrtrack)
            for itrack,track0 in enumerate(avrtrack[0:-1]):
                track1=avrtrack[itrack+1]
                ref1,ref2=avrorigin[itrack]
                ref11,ref12=avrorigin[itrack+1]
                start0,end0=x9yminmax(track0.GetStart(),track0.GetEnd())
                start1,end1=x9yminmax(track1.GetStart(),track1.GetEnd())
                v1=start0.x+1j*start0.y
                v2=end0.x+1j*end0.y
                v3=end1.x+1j*end1.y
                if itrack<(ntrack-2):
                    track2=avrtrack[itrack+2]
                    start2,end2=x9yminmax(track2.GetStart(),track2.GetEnd())
                    v4=end2.x+1j*end2.y
                    rmax=tancen3pr.maxtan(v1,v2,v3,v4)
                    if rmax is not None:
                        
                       pass
                       #  r=rmax
                    #print(itrack,rmax,r)

                #if abs(v2-v1)> minlen and abs(v3-v2)>minlen:
                if (track0.GetClass()=="PCB_TRACK" and track1.GetClass()=="PCB_TRACK" and (abs(numpy.angle(v2-v1)-numpy.angle(v3-v2))>1e-3)):
                    #print(start0,end0,start1,end1)
                    vc,pt1,pt2,pt3=tancen3pr.tancen3pr(v1=v1,v2=v2,v3=v3,r=r)
                    vcp,pt1p,pt2p,pt3p=tancen3pr.roffset(vc,pt1,pt2,pt3,r+dr)
                    vcm,pt1m,pt2m,pt3m=tancen3pr.roffset(vc,pt1,pt2,pt3,r-dr)
                    #print('vc,pt1,pt2,pt3',vc,pt1,pt2,pt3) 
                    avrtrack.append((arc(board=b,start=comp2vec(pt1),mid=comp2vec(pt2),end=comp2vec(pt3),width=width,layer=layer)))
                    track0.SetEnd(comp2vec(pt1))
                    track1.SetStart(comp2vec(pt3))
                    #track0.SetEnd(comp2vec(pt1))
                    #track1.SetStart(comp2vec(pt3))
#                    tobedel.append(track0,track1)
#                    tobeadd.append([('track',(start0,pt1p)),('arc',(pt1p,pt2p,pt3p))])

                  #  track(board=b,start=comp2vec(start0),end=comp2vec(pt1p),width=5*25.4*1000)
                  #  track(board=b,start=comp2vec(start1),end=comp2vec(pt1m),width=5*25.4*1000)
                    a=arc(board=b,start=comp2vec(pt1p),mid=comp2vec(pt2p),end=comp2vec(pt3p),width=width,layer=layer)
                    print(a.GetNetname())
                    a=arc(board=b,start=comp2vec(pt1m),mid=comp2vec(pt2m),end=comp2vec(pt3m),width=width,layer=layer)

                    vrefend1=ref1.GetEnd().x+1j*ref1.GetEnd().y
                    vrefend2=ref2.GetEnd().x+1j*ref2.GetEnd().y
                    if abs(vc-vrefend1)<abs(vc-vrefend2):
                        ref1.SetEnd(comp2vec(pt1m))
                        ref2.SetEnd(comp2vec(pt1p))
                    else:
                        ref1.SetEnd(comp2vec(pt1p))
                        ref2.SetEnd(comp2vec(pt1m))
                    vrefstart1=ref11.GetStart().x+1j*ref11.GetStart().y
                    vrefstart2=ref12.GetStart().x+1j*ref12.GetStart().y
                    if abs(vc-vrefstart1)<abs(vc-vrefstart2):
                        ref11.SetStart(comp2vec(pt3m))
                        ref12.SetStart(comp2vec(pt3p))
                    else:
                        ref11.SetStart(comp2vec(pt3p))
                        ref12.SetStart(comp2vec(pt3m))
                else: 
                    print('length ',abs(v2-v1),abs(v3-v2))
                
                  #  track(board=b,start=comp2vec(start0),end=comp2vec(pt1p),width=5*25.4*1000)
#def track(board,start,end,width):



    elif len(selectednet)==1:
        avrtrack=selectednet[nets[0]]
    #print(avrtrack)
    avrtrack=sorted(avrtrack,key=lambda t:min(x9y(t.GetStart()),x9y(t.GetEnd())))

        #for net,nettrack in selectednet.items():
        #    #print(net,[(t.GetClass(),t.GetStart(),t.GetEnd(),t.GetStart()<t.GetEnd()) for t in selectednet[net]])
        #    for t in nettrack:
        #        if t.GetClass()=='PCB_ARC':
        #            print(net,t.GetMid(),round(t.GetRadius()/25.4),t.GetCenter())
    vias=[]
    for updn in ['up','dn']:
        leftover= 0 #distance*unit
        for t in avrtrack:
            if t.GetClass()=='PCB_TRACK':
                vcomplex,leftover=trackviapos(t,updn=updn,leftoverin=leftover,distance=distance,offset=offset,unit=unit)
                for v in vcomplex:
                    #print(v)
                    vias.append(via(board=b,pos=pcbnew.VECTOR2I(int(round(v.real)),int(round(v.imag))),size=21*mil,drill=7*mil))
                pass
            elif t.GetClass()=='PCB_ARC':
                vcomplex,leftover=arcviapos(arc=t,updn=updn,leftoverin=leftover,distance=distance,offset=offset,unit=unit)
                for v in vcomplex:
                    vias.append(via(board=b,pos=pcbnew.VECTOR2I(int(round(v.real)),int(round(v.imag))),size=21*mil,drill=7*mil))
                pass
            #print(updn, leftover,t.GetClass(),t.GetStart(),t.GetEnd())
            
            #print(t.GetClass(),'updn',updn,'leftover',leftover,t.GetStart(),t.GetEnd(),len(vcomplex))
    if removeavrtrack:
        for t in avrtrack:
            b.Delete(t)

    pcbnew.Refresh()
    gndnet=b.GetNetsByName()['GND']
    vkept=[]
    if removenetvia:
        for v in vias:
            vnet=v.GetNetname()
            if vnet=='' or vnet=='GND':
                v.SetNet(gndnet)
                vkept.append(v)
            else:
                print(vnet)
                b.Delete(v)
    else:
        vkept=vias
    addgroup(board=b,items=vkept)
    pcbnew.Refresh()
    return vias
def trackviapos(track,leftoverin,updn,distance=20,offset=20,unit=mil):
    start=track.GetStart() 
    end=track.GetEnd() 
    startcomplex=start.x+1j*start.y
    endcomplex=end.x+1j*end.y
    diffcomplex=endcomplex-startcomplex
    angle=numpy.angle(diffcomplex)
    length=abs(diffcomplex)
    step=distance*unit
    npoint=int(round(length/step))
    rarray=numpy.arange(-leftoverin,length,step)[1:]
    #print(rarray)
    if updn=='up':
        rarrayc=rarray+1j*offset*unit
    elif updn=='dn':
        rarrayc=rarray-1j*offset*unit
    if len(rarray)>0:
        leftoverout=length-rarray[-1]
    else:
        leftoverout=length
    v=startcomplex+rarrayc*numpy.exp(1j*angle)
    return v,leftoverout
def arcviapos(arc,leftoverin,updn,distance,offset,unit):
    start2I=arc.GetStart() 
    end2I=arc.GetEnd() 
    center2I=arc.GetCenter()
    start2I,end2I=x9yminmax(start2I,end2I)
    radius=arc.GetRadius()
    start=start2I.x+1j*start2I.y
    end=end2I.x+1j*end2I.y
    center=center2I.x+1j*center2I.y
    startvec=start-center
    endvec=end-center
    startangle=numpy.angle(startvec)
    endangle=numpy.angle(endvec)
    diffangle=endangle-startangle
    signdiff=numpy.sign(diffangle)
    if updn=='up':
        r=radius-offset*unit*signdiff
    elif updn=='dn':
        r=radius+offset*unit*signdiff
    else:
        exit(updn)
    leftoverangle=leftoverin/r
    stepangle=distance*unit/r
    a=startangle+numpy.arange(-1*numpy.sign(diffangle)*leftoverangle,diffangle,numpy.sign(diffangle)*stepangle)[1:]
    rarray=r*numpy.exp(1j*a)
    if len(a)>0:
        leftoverout=abs(r*(endangle-a[-1]))
    else:
        leftoverout=abs(endangle-startangle)
    v=center+rarray
    #print('startangle',round(startangle*180/numpy.pi),'endangle',round(endangle*180/numpy.pi),'stepangle',stepangle*180/numpy.pi,(a*180/numpy.pi))
    return v,leftoverout
